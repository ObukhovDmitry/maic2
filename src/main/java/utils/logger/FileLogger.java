package utils.logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileLogger implements Logger {

    private static final String LOG_FILE = "D:\\Events\\MAIC\\log.txt";
    private FileOutputStream out;

    FileLogger() throws IOException {

        Files.deleteIfExists(Paths.get(LOG_FILE));
        Files.createFile(Paths.get(LOG_FILE));
        out = new FileOutputStream(LOG_FILE);
    }

    @Override
    public void info(String msg) {
        log("info : " + msg + "\n");
    }

    @Override
    public void error(String msg) {
        log("error: " + msg + "\n");
    }

    private void log(String msg) {

        Path path = Paths.get(LOG_FILE);
        byte[] bytes = msg.getBytes();

        try {
            out.write(bytes);
        } catch (IOException ignore) { }
    }

    public void close() {

        try {
            out.close();
        } catch (IOException ignore) { }
    }
}
