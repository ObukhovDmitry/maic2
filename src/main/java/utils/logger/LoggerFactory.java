package utils.logger;

import java.io.IOException;

import static utils.Settings.LOGGING_ENABLED;

public class LoggerFactory {

    private static Logger log;

    public static Logger getLogger() {

        if (log == null) {
            initLogger();
        }
        return log;
    }

    public static void finish() {

        if (log.getClass().equals(FileLogger.class)) {
            FileLogger flog = (FileLogger) log;
            flog.close();
        }
    }

    private static void initLogger() {

        if (LOGGING_ENABLED) {
            try {
                log = new FileLogger();
            } catch (IOException e) {
                log = new DumpLogger();
            }
        } else {
            log = new DumpLogger();
        }
    }
}
