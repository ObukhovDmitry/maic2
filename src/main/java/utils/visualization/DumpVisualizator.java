package utils.visualization;

import geom.Point;

import java.util.ArrayList;
import java.util.List;

public class DumpVisualizator implements Visualizator {

    @Override
    public void update() { }

    @Override
    public void drawPoint(Point p) { }

    @Override
    public List<Point> getPoints() {
        return new ArrayList<>();
    }
}
