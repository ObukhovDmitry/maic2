package utils.visualization;

import geom.Point;

import java.util.List;

public interface Visualizator {

    void update();

    void drawPoint(Point p);

    List<Point> getPoints();
}
