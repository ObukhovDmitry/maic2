package utils.visualization;

import static utils.Settings.VISUALIZATION_ENABLED;

public class VisualizatorFactory {

    private static Visualizator vis;

    public static Visualizator getVisualizator() {

        if (vis == null) {
            initLogger();
        }
        return vis;
    }

    private static void initLogger() {

        if (VISUALIZATION_ENABLED) {
            vis = new SwingVisualizator();
        } else {
            vis = new DumpVisualizator();
        }
    }
}
