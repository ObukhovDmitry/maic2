package utils.visualization;

import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import model.GlobalParameters;
import model.top_level.Mine;
import model.top_level.Objects;

import javax.swing.*;
import java.awt.*;

public class ZWindow extends JFrame implements Runnable {

    private static final String WINDOW_NAME = "MiniAICup 2. Leos Visualizer.";
    private ZCanvasPane rootPane;
    private GlobalParameters env;

    public ZWindow(GlobalParameters env) {
        this.env = env;
        setTitle(WINDOW_NAME);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public void start() {
        rootPane = new ZCanvasPane(env);
        rootPane.setDoubleBuffered(false);
        setSize(997, 1020);
        setLayout(new BorderLayout());
        add("Center", rootPane);
        setVisible(true);
    }

    @Override
    public void run() {
        start();
    }

    public void update(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles) {
        rootPane.update(objects, mine, influenceMap, tiles);
        validate();
        getContentPane().repaint();
    }
}