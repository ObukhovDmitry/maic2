package utils.visualization;

import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import geom.Geometry;
import model.GlobalParameters;
import model.mid_level.Fragment;
import model.top_level.Mine;
import model.top_level.Objects;
import utils.JavaCore;
import utils.Settings;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

class ZCanvasPane extends JPanel {

    private static final Logger log = LoggerFactory.getLogger();
    private static final Visualizator vis = VisualizatorFactory.getVisualizator();

    private static final int CANVAS_WIDTH = Settings.FILE_INPUT_ENABLED ? 990 : 660;
    private static final int CANVAS_HEIGHT = Settings.FILE_INPUT_ENABLED ? 990 : 660;

    private GlobalParameters env;
    private Objects objects;
    private Mine mine;
    private InfluenceMap influenceMap;
    private Tiles tiles;

    ZCanvasPane(GlobalParameters env) {
        this.env = env;
    }

    void update(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles) {
        this.objects = objects;
        this.mine = mine;
        this.influenceMap = influenceMap;
        this.tiles = tiles;
    }

    @Override
    public void paint(Graphics g) {

        if (objects == null ) {
            return;
        }

        try {
            drawTiles(g);
            //drawGrid(g);
            drawMe(g);
            drawOp(g);
            drawTrajectory(g);
            drawVis(g);
            drawFood(g);
            drawTarget(g);
        } catch (Exception ignore) {}
    }

    private void drawFood(Graphics g) {
        g.setColor(new Color(0, 128, 0));
        objects.getFoods().forEach(p -> {
            double x0 = Math.min(Math.max(p.getX() - 2, 0), env.GAME_WIDTH - 4);
            double y0 = Math.min(Math.max(p.getY() - 2, 0), env.GAME_HEIGHT - 4);
            g.fillRect((int) x0, (int) y0, 4, 4);
        });
    }

    private void drawVis(Graphics g) {
        g.setColor(Color.BLUE);
        vis.getPoints().forEach(p -> {
            double x0 = Math.min(Math.max(p.getX() - 2, 0), env.GAME_WIDTH - 4);
            double y0 = Math.min(Math.max(p.getY() - 2, 0), env.GAME_HEIGHT - 4);
            g.fillRect((int) x0, (int) y0, 4, 4);
        });
    }

    private void drawGrid(Graphics g) {
        for (int i = 0; i < Tiles.N_TILES; i++) {
            int x = (int) (CANVAS_WIDTH * (i + 1) / Tiles.N_TILES);
            int y = (int) (CANVAS_HEIGHT * (i + 1)/ Tiles.N_TILES);
            g.drawLine(x, 0, x, CANVAS_HEIGHT);
            g.drawLine(0, y, CANVAS_WIDTH, y);
        }
    }

    private void drawTiles(Graphics g) {

        for (int i = 0; i < Tiles.N_TILES; i++) {
            for (int j = 0; j < Tiles.N_TILES; j++) {

                int x0 = (int) (CANVAS_WIDTH * i / Tiles.N_TILES);
                int y0 = (int) (CANVAS_HEIGHT * j/ Tiles.N_TILES);
                int x1 = (int) (CANVAS_WIDTH * (i + 1) / Tiles.N_TILES);
                int y1 = (int) (CANVAS_HEIGHT * (j + 1)/ Tiles.N_TILES);

                double x = (x0 + x1) / 2;
                double y = (y0 + y1) / 2;

                //float w = (float) influenceMap.getFoodPotential(x, y) / 3;
                float w = (float) influenceMap.getTracePotential(x, y) * 1;

                w = w > 1.0f ? 1.0f : (w < 0 ? 0 : w);

                g.setColor(new Color(1.0f - w, 1.0f, 1.0f - w));
                g.fillRect(x0, y0, x1 - x0, y1 - y0);
            }
        }
    }

    private void drawTarget(Graphics g) {

        double x0 = Math.min(Math.max(mine.getCommandX() - 2, 0), env.GAME_WIDTH - 4);
        double y0 = Math.min(Math.max(mine.getCommandY() - 2, 0), env.GAME_HEIGHT - 4);

        g.setColor(Color.RED);
        g.fillRect((int) x0, (int) y0, 4, 4);
    }

    private void drawMe(Graphics g) {

        Color color = mine.isSplitPossible() ? new Color(127, 0, 0) : Color.RED;
        g.setColor(color);

        for (Fragment me: mine.getMe()) {;
            int r = (int) me.getRadius();
            g.fillOval((int) me.getX() - r, (int) me.getY() - r, 2 * r, 2 * r );
        }

        // Радиус обзора
        g.setColor(Color.BLACK);

        if (mine.getMe().size() == 1) {
            Fragment me0 = mine.getMe().get(0);

            double sx = me0.getSx();
            double sy = me0.getSy();
            double s = Math.sqrt(sx * sx + sy * sy);

            double vCx = me0.getX() + env.VISION_SHIFT * sx / s ;
            double vCy = me0.getY() + env.VISION_SHIFT * sy / s ;
            double vR = env.VISION_SINGLE_COEF * me0.getRadius();

            g.drawOval((int) (vCx - vR), (int) (vCy - vR), (int) (2 * vR), (int) (2 * vR));

        } else if (mine.getMe().size() > 1) {
            double visionCoef = env.VISION_MULTI_COEF * Math.sqrt(mine.getMe().size());
            for (Fragment me0: mine.getMe()) {
                double sx = me0.getSx();
                double sy = me0.getSy();
                double s = Math.sqrt(sx * sx + sy * sy);

                double vCx = me0.getX() + env.VISION_SHIFT * sx / s ;
                double vCy = me0.getY() + env.VISION_SHIFT * sy / s ;
                double vR = visionCoef * me0.getRadius();

                g.drawOval((int) (vCx - vR), (int) (vCy - vR), (int) (2 * vR), (int) (2 * vR));
            }
        }
    }

    private void drawOp(Graphics g) {

        for (Fragment op: objects.getDangerOp()) {;

            int r = (int) op.getRadius();
            g.setColor(Color.ORANGE);
            g.fillOval((int) op.getX() - r, (int) op.getY() - r, 2 * r, 2 * r );
        }
    }

    private void drawTrajectory(Graphics g) {

        double dAngle = 2.0 * Math.PI / (1.0 * env.N_DIRECTIONS);
        g.setColor(Color.BLACK);

        for (Fragment me: mine.getMe()) {
            for (int i = 0; i < env.N_DIRECTIONS; i++) {
                double angle = i * dAngle;
                boolean[] eatens = new boolean[objects.getFoods().size()];
                int[] willEaten = {-1};
                drawTrajectoryOfAngle(me, angle, 0, eatens, willEaten, g);
            }
            //break;
        }
    }

    public double drawTrajectoryOfAngle(Fragment me, double angle, int deep, boolean[] eatens,
                                        int[] willEaten, Graphics g) {

        // оригинал: PerformanceCalculator:doPredict

        int deepCheckpointId = 0;
        while (deepCheckpointId < env.DEEP_CHECKPOINTS.size() && env.DEEP_CHECKPOINTS.get(deepCheckpointId) <= deep) {
            deepCheckpointId++;
        }
        if (deepCheckpointId >= env.DEEP_CHECKPOINTS.size()) {
            return 0;
        }
        int MAX_N_STRAIGHT_PREDICTS = 4;
        int step = Math.max(1, 1 + (env.DEEP_CHECKPOINTS.get(deepCheckpointId) - deep - 1) / MAX_N_STRAIGHT_PREDICTS);

        final double angleCopy = JavaCore.normalizeAngleInRadians(angle);
        int deepCopy = deep;
        boolean[] eatensCopy = eatens.clone();
        int[] willEatenRes = {-1};
        boolean end = false;
        double curCost = 0;
        Fragment me0 = me;

        do  {
            Fragment pred = me0.predictFragmentByAngle(angleCopy, step, env);

            //if (!mine.inVisionRange(pred.getX(), pred.getY())) {
            //    end = true;
            //    break;
            //}

            double deepRatio = Math.pow(env.DECREASE_RATIO, deep);

            // съеденный food
            List<Integer> curEatens = me0.predictEaten(pred.getX(), pred.getY(), tiles, env);
            for (Integer j: curEatens) {
                if (eatensCopy.length > j && !eatensCopy[j]) {
                    curCost += deepRatio;
                    eatensCopy[j] = true;
                    log.info("ZCanvasPane.java: " + deepRatio);
                }
            }

            // defence players
            for (Fragment op: objects.getOp()) {

                if (op.getMass()+ env.FOOD_MASS < env.MASS_EAT_FACTOR * pred.getMass()) {
                    continue;
                }

                int k = deep + 1;
                double maxSpeed = env.SPEED_FACTOR / Math.sqrt(op.getMass());
                double alpha = env.INERTION_FACTOR / op.getMass();
                double alpha1 = 1.0 - alpha;
                double alpha2 = 1.0 - Math.pow(alpha1, k);
                double alpha3 = alpha1 * alpha2 / alpha;
                double opX = op.getX() + alpha3 * op.getSx();
                double opY = op.getY() + alpha3 * op.getSy();
                double opVariance = (k - alpha3) * maxSpeed;
                double opRadius = op.getRadius() + opVariance;
                double dist = Geometry.distance(pred.getX(), pred.getY(), opX, opY);

                if (dist < opRadius) {
                    curCost -= env.SCORE_FOR_PLAYER * deepRatio;
                    willEatenRes[0] = deep + step;
                    break;
                }
            }

            if (willEatenRes[0] != -1) {
                end = true;
                break;
            }

            // self collisions
            if (env.DEEP_CHECKPOINTS.size() > 1 && deep < env.DEEP_CHECKPOINTS.get(1) && objects.getDangerOp().size() == 0) {
                // check it
                for (Fragment me1: mine.getMe()) {
                    if (me1.getfId() == pred.getfId()) {
                        continue;
                    }

                    double px = pred.getX();
                    double py = pred.getY();
                    double dot = Geometry.dotprod(me0.getX() - px, me0.getY() - py, me1.getX() - px, me1.getY() - py);
                    double pm0 = Geometry.distance(px, py, me0.getX(), me0.getY());
                    double pm1 = Geometry.distance(px, py, me1.getX(), me1.getY());
                    double cos = dot / (pm0 * pm1);

                    if (cos < -0.8 && pm1 < me0.getRadius() + pred.getRadius()) {
                        end = true;
                        break;
                    }
                }
            }
            if (end) {
                break;
            }

            me0 = pred;
            deep += step;
        } while (deep < env.DEEP_CHECKPOINTS.get(deepCheckpointId));

        double maxCost = Double.NEGATIVE_INFINITY;
        double minCost = Double.POSITIVE_INFINITY;

        if (deep < env.MAX_PREDICT_DEEP && !end) {

            List<Double> newAngles = new ArrayList<Double>() {{
                add(angleCopy);
            }};
            if (me0.getSpeed() < env.SPEED_FACTOR / Math.sqrt(me0.getMass())) {
                newAngles.add(angleCopy + Math.PI / 4.0);
                newAngles.add(angleCopy - Math.PI / 4.0);
            }

            for (double newAngle : newAngles) {
                int[] willEatenCopy2 = {-1};
                Fragment pred = me0.predictFragmentByAngle(newAngle, env);

                double cost = drawTrajectoryOfAngle(pred, newAngle, deep, eatensCopy, willEatenCopy2, g);

                if (cost > maxCost) {
                    maxCost = cost;
                    willEatenRes[0] = willEatenCopy2[0];
                }
                if (cost < minCost) {
                    minCost = cost;
                }
            }

            minCost += curCost;
            maxCost += curCost;

        } else {
            maxCost = curCost;
            minCost = curCost;
        }

        willEaten[0] = willEatenRes[0];

        //if (maxCost > 0) {
            g.setColor(Color.BLACK);

            Fragment me1 = me;
            do  {
                Fragment pred = me1.predictFragmentByAngle(angleCopy, step, env);

                g.drawLine((int) me1.getX(), (int) me1.getY(), (int) pred.getX(), (int) pred.getY());

                me1 = pred;
                deepCopy += step;
            } while (deepCopy < env.DEEP_CHECKPOINTS.get(deepCheckpointId));
        //}

        return maxCost;
    }
}
