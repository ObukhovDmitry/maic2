package utils.visualization;

import geom.Point;

import java.util.ArrayList;
import java.util.List;

public class SwingVisualizator implements Visualizator {

    private List<Point> points;


    public SwingVisualizator() {}


    @Override
    public void update() {
        points = new ArrayList<>();
    }

    @Override
    public void drawPoint(Point p) {
        points.add(p);
    }

    @Override
    public List<Point> getPoints() {
        return points;
    }
}
