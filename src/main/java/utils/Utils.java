package utils;

import geom.Point;
import model.GlobalParameters;
import model.mid_level.Fragment;
import model.top_level.Mine;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    private static final Logger log = LoggerFactory.getLogger();

    public static List<Point> getPointsOnDirection(double x0, double y0, double x1, double y1, GlobalParameters env) {

        int maxPointsCount = 4;          // todo magic constants
        double minSegment = 150.0;

        // направляющая x150
        double nx = x1 - x0;
        double ny = y1 - y0;
        double nn = Math.sqrt(nx * nx + ny * ny);

        if (Double.compare(nn, 0) == 0) {
            throw new UnsupportedOperationException("Utils.java: (x0, y0), (x1, x1) are same!");
        }

        nx *= minSegment / nn;
        ny *= minSegment / nn;

        // дистанция до границы, в ед. x150
        double kx = nx > 0 ? (env.GAME_WIDTH - x0)  / nx : -x0 / nx;
        double ky = ny > 0 ? (env.GAME_HEIGHT - y0) / ny : -y0 / ny;
        double k = Math.min(kx, ky);

        // дистаниця до границы, в обычных ед.
        double dist2b = k * minSegment;

        // определение кол-ва точек, длины сегмента
        int pointsCount = Math.max(1, Math.min((int) k, maxPointsCount));
        double segment = dist2b / pointsCount;

        // формируем список точек
        nx *= segment / minSegment;
        ny *= segment / minSegment;

        List<Point> points = new ArrayList<>(pointsCount);
        for (int i = 0; i < pointsCount; i++) {
            double x = x0 + (i + 1) * nx;
            double y = y0 + (i + 1) * ny;
            points.add(new Point(x, y));
        }

        return points;
    }

    public static double[] prepareAnglesForCostProcessor(Mine mine, List<Point> targets) {

        double MAX_D_ANGLE = Math.PI;       // todo magic constant

        // все возможные углы
        List<Double> angles = new ArrayList<>();

        for (Fragment me: mine.getMe()) {
            for (Point p: targets) {
                double angle = Math.atan2(p.getY() - me.getY(), p.getX() - me.getX());
                angles.add(angle);
            }
        }

        angles.sort(Double::compare);

        // максимальное приращение между углами
        double maxDa = Double.NEGATIVE_INFINITY;
        int skipIndex = -1;

        for (int i = 0; i < angles.size(); i++) {
            int j = (i + 1) % angles.size();
            double da = angles.get(j) - angles.get(i);
            if (da < 0) {
                da += 2.0 * Math.PI;
            }
            if (maxDa < da) {
                maxDa = da;
                skipIndex = i;
            }
        }

        // диапазон, в котором перебирать углы
        double[] anglesDiapason = null;

        if (maxDa > MAX_D_ANGLE) {
            anglesDiapason = new double[2];
            anglesDiapason[0] = angles.get((skipIndex + 1) % angles.size());
            anglesDiapason[1] = angles.get(skipIndex);
        }

        return anglesDiapason;
    }
}
