package utils;

public class JavaCore {

    public static int countOfTrueInBooleanArray(boolean[] array) {

        int res = 0;

        for (boolean element : array) {
            if (element) {
                res++;
            }
        }

        return res;
    }

    public static double normalizeAngleInRadians(double angle) {

        while (angle < 0) {
            angle += 2.0 * Math.PI;
        }
        while (angle >= 2.0 * Math.PI) {
            angle -= 2.0 * Math.PI;
        }

        return angle;
    }
}
