package utils;

public class Settings {
    public static final boolean VISUALIZATION_ENABLED = false;
    public static final boolean LOGGING_ENABLED = false;
    public static final boolean FILE_INPUT_ENABLED = false;
}
