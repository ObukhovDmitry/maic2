package algo.controllers;

import algo.PlayerAnalyzer;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import model.GlobalParameters;
import model.StrategyType;
import model.top_level.Mine;
import model.top_level.Objects;
import org.json.JSONObject;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

public class MainController implements IController {

    private static final Logger log = LoggerFactory.getLogger();

    private GlobalParameters env;
    private FarmController farmController;
    private AttackController attackController;
    private StrategyType strategyType;

    private int tickWhenStrategyChanged;

    public MainController(
            GlobalParameters env,
            FarmController farmController,
            AttackController attackController) {

        this.env = env;
        this.farmController = farmController;
        this.attackController = attackController;
        strategyType = StrategyType.FARM;
    }

    @Override
    public JSONObject handle(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        JSONObject command = null;

        updateStrategy(objects, mine, influenceMap, tiles, playerAnalyzer);

        switch (strategyType) {
            case FARM:
                log.info("MainController.java: FARM");
                command = farmController.handle(objects, mine, influenceMap, tiles, playerAnalyzer);
                break;

            case ATTACK:
                log.info("MainController.java: ATTACK");
                command = attackController.handle(objects, mine, influenceMap, tiles, playerAnalyzer);
                break;

            case SKIP:
                log.info("MainController.java: SKIP");
                command = new JSONObject();
                command.put("X", mine.getCommandX());
                command.put("Y", mine.getCommandY());
                break;
        }

        return command;
    }


    private void updateStrategy(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        StrategyType before = strategyType;

        if (canSkip(mine, objects)) {
            strategyType = StrategyType.SKIP;

        } else if (canAttack(objects, mine, influenceMap, tiles, playerAnalyzer)) {
            strategyType = StrategyType.ATTACK;

        } else {
            strategyType = StrategyType.FARM;
        }

        if (before != strategyType) {
            tickWhenStrategyChanged = objects.getTickIndex();
        }
    }

    private boolean canSkip(Mine mine, Objects objects) {

        //int lastSkipLength = strategyType == StrategyType.SKIP ?
        //        objects.getTickIndex() - tickWhenStrategyChanged : 0;

        return //lastSkipLength < env.DEEP_CHECKPOINTS.get(0) &&
                strategyType != StrategyType.SKIP &&
                !objects.isChanged() &&
                !mine.inVisionRange(mine.getCommandX(), mine.getCommandY());
    }

    private boolean canAttack(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {
        return attackController.canHandle(objects, mine, influenceMap, tiles, playerAnalyzer);
    }
}
