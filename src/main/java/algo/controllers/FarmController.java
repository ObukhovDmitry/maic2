package algo.controllers;

import algo.PlayerAnalyzer;
import algo.processors.FarmProcessor;
import geom.Point;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import model.GlobalParameters;
import model.top_level.Mine;
import model.top_level.Objects;
import org.json.JSONObject;
import utils.logger.Logger;
import utils.logger.LoggerFactory;


public class FarmController implements IController {

    private static final Logger log = LoggerFactory.getLogger();

    private GlobalParameters env;
    private FarmProcessor farmProcessor;
    private FarmProcessor splitProcessor;

    public FarmController(GlobalParameters env) {
        this.env = env;
    }

    @Override
    public JSONObject handle(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        if (mine.isSplitPossible()) {
            handleSplit(objects, mine, influenceMap, tiles, playerAnalyzer);

        } else {
            handleDefault(objects, mine, influenceMap, tiles, playerAnalyzer);
        }

        JSONObject command = new JSONObject();
        command.put("X", mine.getCommandX());
        command.put("Y", mine.getCommandY());
        if (mine.isSplit()) {
            command.put("Split", true);
        }

        return command;
    }

    private void handleDefault(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        farmProcessor = new FarmProcessor(env, objects, mine, influenceMap, tiles, playerAnalyzer);
        farmProcessor.preprocess();
        farmProcessor.process();

        Point command = farmProcessor.getCommand();

        mine.setCommandX(command.getX());
        mine.setCommandY(command.getY());
        mine.setSplit(false);

    }

    private void handleSplit(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        // without split
        farmProcessor = new FarmProcessor(env, objects, mine, influenceMap, tiles, playerAnalyzer);
        farmProcessor.preprocess();
        farmProcessor.process();

        double performanceWithoutSplit = farmProcessor.getPerformance();

        log.info("FarmController.java: without split: " + performanceWithoutSplit);

        // with split
        Mine mineSplit = mine.predictMineSplit(mine.getCommandX(), mine.getCommandY());
        Objects objects1 = objects.predictNextTick();

        splitProcessor = new FarmProcessor(env, objects1, mineSplit, influenceMap, tiles, playerAnalyzer);
        splitProcessor.preprocess();
        splitProcessor.process();

        double performanceWithSplit = splitProcessor.getPerformance();

        log.info("FarmController.java: with    split: " + performanceWithSplit);

        // choose the best
        boolean doSplit = performanceWithSplit > performanceWithoutSplit + env.SCORE_FOR_FOOD;
        Point command = doSplit ? splitProcessor.getCommand() : farmProcessor.getCommand();

        mine.setCommandX(command.getX());
        mine.setCommandY(command.getY());
        mine.setSplit(doSplit);
    }
}
