package algo.controllers;

import algo.PlayerAnalyzer;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import model.top_level.Mine;
import model.top_level.Objects;
import org.json.JSONObject;

public interface IController {
    JSONObject handle(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer);
}
