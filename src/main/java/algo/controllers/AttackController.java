package algo.controllers;

import algo.PlayerAnalyzer;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import geom.Point;
import model.AttackStrategyType;
import model.GlobalParameters;
import model.top_level.Mine;
import model.top_level.Objects;
import org.json.JSONObject;
import algo.processors.AttackProcessor;
import utils.logger.Logger;
import utils.logger.LoggerFactory;


public class AttackController implements IController {

    private static final Logger log = LoggerFactory.getLogger();

    private GlobalParameters env;
    private AttackProcessor attackProcessor;
    private AttackProcessor splitProcessor;
    private AttackStrategyType attackStrategyType;

    public AttackController(GlobalParameters env) {
        this.env = env;
    }

    @Override
    public JSONObject handle(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        updateAttackStrategyType(mine, objects, influenceMap, tiles, playerAnalyzer);

        switch (attackStrategyType) {
            case CHASE:
                handleChase(mine);
                break;

            case SPLIT:
                handleSplit(mine);
                break;

            case FUSE:
                handleFuse(mine);
                break;
        }

        JSONObject command = new JSONObject();
        command.put("X", mine.getCommandX());
        command.put("Y", mine.getCommandY());
        if (mine.isSplit()) {
            command.put("Split", true);
        }

        return command;
    }

    public boolean canHandle(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        attackProcessor = new AttackProcessor(env, objects, mine, influenceMap, tiles, playerAnalyzer);
        attackProcessor.preprocess();

        return attackProcessor.getTargets().size() > 0;
    }


    private void updateAttackStrategyType(Mine mine, Objects objects, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        if (isSplitAttackPossible(objects, mine, influenceMap, tiles, playerAnalyzer)) {
            attackStrategyType = AttackStrategyType.SPLIT;

        } else if (isFuseAttackPossible()) {
            attackStrategyType = AttackStrategyType.FUSE;

        } else {
            attackStrategyType = AttackStrategyType.CHASE;
        }
    }

    private boolean isSplitAttackPossible(Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {

        if (!mine.isSplitPossible()) {
            return false;
        }

        Mine mineSplit = mine.predictMineSplit(mine.getCommandX(), mine.getCommandY());
        Objects objects1 = objects.predictNextTick();

        splitProcessor = new AttackProcessor(env, objects1, mineSplit, influenceMap, tiles, playerAnalyzer);
        splitProcessor.preprocess();

        return splitProcessor.getTargets().size() > 0;
    }

    private boolean isFuseAttackPossible() {

        // todo implement

        return false;
    }


    private void handleChase(Mine mine) {

        attackProcessor.process();
        Point command = attackProcessor.getCommand();

        mine.setCommandX(command.getX());
        mine.setCommandY(command.getY());
        mine.setSplit(false);
    }

    private void handleSplit(Mine mine) {

        attackProcessor.process();
        double performanceWithoutSplit = attackProcessor.getPerformance();

        splitProcessor.process();
        double performanceWithSplit = splitProcessor.getPerformance();

        boolean doSplit = performanceWithSplit > performanceWithoutSplit;
        Point command = doSplit ? splitProcessor.getCommand() : attackProcessor.getCommand();

        mine.setCommandX(command.getX());
        mine.setCommandY(command.getY());
        mine.setSplit(doSplit);
    }

    private void handleFuse(Mine mine) {

        // todo implement

    }
}
