package algo;

import algo.PlayerAnalyzer;
import model.tiles.InfluenceMap;
import model.mid_level.Food;
import model.tiles.Tiles;
import model.*;
import model.mid_level.Fragment;
import model.mid_level.Virus;
import model.top_level.Mine;
import model.top_level.Objects;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Preprocessor {

    private final Logger log = LoggerFactory.getLogger();

    public GlobalParameters getGlobalParameters(JSONObject config) {
        return new GlobalParameters(
                config.getInt("GAME_TICKS"),
                config.getInt("GAME_HEIGHT"),
                config.getInt("GAME_WIDTH"),
                config.getDouble("INERTION_FACTOR"),
                config.getDouble("VISCOSITY"),
                config.getDouble("SPEED_FACTOR"),
                config.getDouble("FOOD_MASS"),
                config.getDouble("VIRUS_RADIUS"),
                config.getDouble("VIRUS_SPLIT_MASS"),
                config.getInt("MAX_FRAGS_CNT"),
                config.getInt("TICKS_TIL_FUSION"));
    }

    public void updateMine(JSONArray meJson, Mine mine) {

        List<Fragment> me = new ArrayList<>(16);

        for (int i = 0; i < meJson.length(); i++) {
            JSONObject obj = meJson.getJSONObject(i);
            String id = obj.getString("Id");
            int pId = Integer.parseInt(id.substring(0, 1));
            int fId = id.length() > 1 ? Integer.parseInt(id.substring(2)) : 0;
            int ttf = obj.has("TTF") ? obj.getInt("TTF") : 0;

            me.add(new Fragment(
                    pId,
                    fId,
                    obj.getDouble("X"),
                    obj.getDouble("Y"),
                    obj.getDouble("M"),
                    obj.getDouble("R"),
                    obj.getDouble("SX"),
                    obj.getDouble("SY"),
                    ttf));
        }

        mine.setMe(me);
    }

    public void updateObjects(JSONArray objJson, Objects objects, Mine mine, GlobalParameters env) {

        objects.setChanged(false);

        List<Food> foods = new ArrayList<>(256);
        List<Virus> viruses = new ArrayList<>(32);
        List<Fragment> op = new ArrayList<>(32);

        for (int i = 0; i < objJson.length(); i++) {
            JSONObject obj = objJson.getJSONObject(i);

            if (obj.getString("T").equals("F")) {
                foods.add(new Food(foods.size(), obj.getDouble("X"), obj.getDouble("Y"), objects.getTickIndex()));

            } else if (obj.getString("T").equals("V")) {
                viruses.add(new Virus(
                        obj.getString("Id"),
                        obj.getDouble("M"),
                        obj.getDouble("X"),
                        obj.getDouble("Y")));

            } else if (obj.getString("T").equals("P")) {
                String id = obj.getString("Id");
                int pId = Integer.parseInt(id.substring(0, 1));
                int fId = id.length() > 1 ? Integer.parseInt(id.substring(2)) : 0;

                op.add(new Fragment(
                        pId, fId,
                        obj.getDouble("X"),
                        obj.getDouble("Y"),
                        obj.getDouble("M"),
                        obj.getDouble("R")));
            }
        }

        objects.updateFoods(mine, foods);
        objects.setViruses(viruses);
        objects.updateOp(op, mine, env);
    }

    public void updateTiles(Tiles tiles, Objects objects, Mine mine) {

        tiles.prepareToUpdate();

        objects.getEatenFoods().forEach(x -> tiles.updateWithEatenFood(x, objects.getTickIndex()));
        objects.getFoods().forEach(x -> tiles.updateWithActualFood(x, objects.getTickIndex()));

        tiles.updateLastUpdate(mine, objects.getTickIndex());
    }

    public void updateInfluenceMap(InfluenceMap influenceMap, Tiles tiles, Objects objects, Mine mine) {
        influenceMap.update(objects);
    }

    public void updatePlayerAnalyzer(PlayerAnalyzer playerAnalyzer, Objects objects, Mine mine) {
        playerAnalyzer.update(objects, mine);
    }
}
