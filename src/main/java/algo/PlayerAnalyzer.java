package algo;

import geom.Geometry;
import model.GlobalParameters;
import model.PlayerStyle;
import model.mid_level.Fragment;
import model.top_level.Mine;
import model.top_level.Objects;

public class PlayerAnalyzer {

    private GlobalParameters env;


    public PlayerAnalyzer(GlobalParameters env) {
        this.env = env;
    }


    public Fragment predictPosition(Fragment op, int nTicks) {

        PlayerStyle playerStyle = getPlayerStyle(op);

        switch(playerStyle) {

            case AFK:
                return op;

            case DEFAULT:
                return predictPositionDefault(op, nTicks);
        }

        throw new UnsupportedOperationException("PlayerAnalyzer.java: undefined player style");
    }

    public Fragment predictPositionDefault(Fragment op, int nTicks) {

        // профилактика, рекурсия все-таки
        if (nTicks <= 0) {
            return new Fragment(op.getpId(), op.getfId(), op.getX(), op.getY(), op.getMass(), op.getRadius(),
                    op.getSx(), op.getSy(), op.getTtf());
        }

        double speed = op.getSpeed();
        double maxSpeed = env.SPEED_FACTOR / Math.sqrt(op.getMass());

        if (speed > maxSpeed) {
            // handle fast
            int nStraightTicks = (int) ((speed - maxSpeed) / env.VISCOSITY) + 1;
            nStraightTicks = Math.min(nStraightTicks, nTicks);

            double nx = op.getSx() / speed;
            double ny = op.getSy() / speed;

            double speedPred = speed - nStraightTicks * env.VISCOSITY;
            double sxPred = nx * speedPred;
            double syPred = ny * speedPred;

            double xPred = op.getX() + nStraightTicks * op.getSx() - nx * env.VISCOSITY * nStraightTicks * (nStraightTicks + 1) / 2.0;
            double yPred = op.getY() + nStraightTicks * op.getSy() - ny * env.VISCOSITY * nStraightTicks * (nStraightTicks + 1) / 2.0;

            if (xPred - op.getRadius() < 0) { xPred = op.getRadius(); sxPred = 0; }
            if (yPred - op.getRadius() < 0) { yPred = op.getRadius(); syPred = 0; }
            if (xPred + op.getRadius() > env.GAME_WIDTH) { xPred = env.GAME_WIDTH - op.getRadius(); sxPred = 0; }
            if (yPred + op.getRadius() > env.GAME_HEIGHT) { yPred = env.GAME_HEIGHT - op.getRadius(); syPred = 0; }

            double massPred = op.getMass(); // todo учесть съеденный фууд
            Fragment pred = new Fragment(op.getpId(), op.getfId(), xPred, yPred, massPred, env.mass2radius(massPred),
                    sxPred, syPred, op.getTtf() - nStraightTicks);

            return predictPositionDefault(pred, nTicks - nStraightTicks);

        } else {

            double alpha = env.INERTION_FACTOR / op.getMass();
            double alpha1 = 1.0 - alpha;
            double alpha2 = 1.0 - Math.pow(alpha1, nTicks);
            double alpha3 = alpha1 * alpha2 / alpha;
            double xPred = op.getX() + alpha3 * op.getSx();
            double yPred = op.getY() + alpha3 * op.getSy();
            double sxPred = 0;    // todo заполнить в соотв с формулами
            double syPred = 0;    // todo заполнить в соотв с формулами
            double opVariance = (nTicks - alpha3) * maxSpeed;
            double opRadius = op.getRadius() + opVariance;      // здесь по идее д.б. ещё минус 1/3 myRadius
            double massPred = op.getMass(); // todo учесть съеденный фууд и тп

            return new Fragment(op.getpId(), op.getfId(), xPred, yPred, massPred, opRadius,
                    sxPred, syPred, op.getTtf() - nTicks);
        }
    }

    public boolean canDodge(Fragment op) {

        PlayerStyle playerStyle = getPlayerStyle(op);

        switch(playerStyle) {

            case AFK:
                return false;

            case DEFAULT:
                return false;
        }

        throw new UnsupportedOperationException("PlayerAnalyzer.java: undefined player style");
    }

    public void update(Objects objects, Mine mine) {

        // todo implement
    }

    private PlayerStyle getPlayerStyle(Fragment fragment) {

        // todo implement

        return PlayerStyle.DEFAULT;
    }
}
