package algo;

import geom.Geometry;
import geom.Point;
import model.mid_level.Virus;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import model.GlobalParameters;
import model.mid_level.Fragment;
import model.top_level.Mine;
import model.top_level.Objects;
import utils.JavaCore;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class PerformanceCalculator {

    private static final Logger log = LoggerFactory.getLogger();

    private static final double PENALTY_DIR_CHANGE = 1e-3;

    private GlobalParameters env;
    private Objects objects;
    private Mine mine;
    private InfluenceMap influenceMap;
    private Tiles tiles;
    private PlayerAnalyzer playerAnalyzer;

    private List<List<int[]>> eatenFoods;   // фрагменты, направления, food
    private List<int[]> willEaten;      // фрагменты, направления
    private List<int[]> willEat;      // фрагменты, направления
    private double[] anglesDiapason;
    private List<Double> angles;


    public PerformanceCalculator(GlobalParameters env, Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {
        this.env = env;
        this.objects = objects;
        this.mine = mine;
        this.influenceMap = influenceMap;
        this.tiles = tiles;
        this.playerAnalyzer = playerAnalyzer;
    }

    public void setAnglesDiapason(double[] anglesDiapason) {
        this.anglesDiapason = anglesDiapason;

        if (anglesDiapason != null) {
            anglesDiapason[0] = anglesDiapason[0] < 0 ? anglesDiapason[0] + 2.0 * Math.PI : anglesDiapason[0];
            anglesDiapason[1] = anglesDiapason[1] < 0 ? anglesDiapason[1] + 2.0 * Math.PI : anglesDiapason[1];
        }
    }

    private void initAngles() {

        // особый случай - 1 направление
        if (anglesDiapason != null && Double.compare(anglesDiapason[0], anglesDiapason[1]) == 0) {
            angles = new ArrayList<Double>() {{ add(anglesDiapason[0]); }};
            return;
        }

        double baseAngle = anglesDiapason == null ? 0 : anglesDiapason[0];
        double sector = anglesDiapason == null ? 2.0 * Math.PI : anglesDiapason[1] - anglesDiapason[0];
        sector = sector < 0 ? sector + 2.0 * Math.PI : sector;

        int numberOfDirections =
                sector < Math.PI / 4.0 ? 2 :
                        sector < Math.PI / 2.0 ? 4 :
                                sector < Math.PI ? 6 : 8;       // todo magic constants
        numberOfDirections = Math.min(numberOfDirections, env.N_DIRECTIONS);
        int numberOfIntervals = anglesDiapason == null ? numberOfDirections : numberOfDirections - 1;

        angles =  new ArrayList<>(numberOfDirections);

        for (int i = 0; i < numberOfDirections; i++) {
            double angle = baseAngle + sector * i / (1.0 * numberOfIntervals);
            angle = angle > 2.0 * Math.PI ? angle - 2.0 * Math.PI : angle;

            angles.add(angle);
        }
    }

    public void fit() {

        initAngles();

        eatenFoods = new ArrayList<>(mine.getMe().size());
        for (int i = 0; i < mine.getMe().size(); i++) {
            eatenFoods.add(new ArrayList<>(angles.size()));
            for (int j = 0; j < angles.size(); j++) {
                eatenFoods.get(i).add(new int[objects.getFoods().size()]);
            }
        }

        willEaten = new ArrayList<>(mine.getMe().size());
        for (int i = 0; i < mine.getMe().size(); i++) {
            willEaten.add(new int[angles.size()]);
        }

        willEat = new ArrayList<>(mine.getMe().size());
        for (int i = 0; i < mine.getMe().size(); i++) {
            willEat.add(new int[angles.size()]);
        }

        for (int i = 0; i < mine.getMe().size(); i++) {
            for (int j = 0; j < angles.size(); j++) {
                double angle = angles.get(j);
                int[] tmp1 = {-1};
                int[] tmp2 = {-1};

                doPredict(mine.getMe().get(i), angle, 0, eatenFoods.get(i).get(j), tmp1, tmp2);

                willEaten.get(i)[j] = tmp1[0];
                willEat.get(i)[j] = tmp2[0];
            }
        }

        //for (int j = 0; j < angles.size(); j++) {
        //    log.info("PerformanceCalculator.java: angle=" + angles.get(j) + ", w1=" + W1.get(0)[j]);
        //}
    }


    public double calcCost(Point p) {

        double opCost = calcOpCost(p.getX(), p.getY());
        double foodCost = calcFoodCost(p.getX(), p.getY());
        double virusCost = calcVirusCost(p.getX(), p.getY());
        double traceCost = calcW1(p.getX(), p.getY());

        return opCost + foodCost + virusCost + traceCost;
    }


    public double calcOpCost(double x, double y) {

        double res = 0;

        for (int i = 0; i < mine.getMe().size(); i++) {

            Fragment me = mine.getMe().get(i);

            double angle = Math.atan2((y - me.getY()), (x - me.getX()));
            angle = angle < 0 ? angle + 2.0 * Math.PI : angle;

            double angleDoubleId = getAngleId(angle);

            int i1 = (int) angleDoubleId;
            int i2 = (i1 + 1) % angles.size();

            double w1 = i1 + 1.0 - angleDoubleId;
            double w2 = angleDoubleId - i1;

            double wn1 = willEaten.get(i)[i1] == -1 ? 0 : env.SCORE_FOR_PLAYER * Math.pow(env.DECREASE_RATIO, willEaten.get(i)[i1]);
            double wn2 = willEaten.get(i)[i2] == -1 ? 0 : env.SCORE_FOR_PLAYER * Math.pow(env.DECREASE_RATIO, willEaten.get(i)[i2]);
            double wp1 = willEat.get(i)[i1] == -1 ? 0 : env.SCORE_FOR_PLAYER * Math.pow(env.DECREASE_RATIO, willEat.get(i)[i1]);
            double wp2 = willEat.get(i)[i2] == -1 ? 0 : env.SCORE_FOR_PLAYER * Math.pow(env.DECREASE_RATIO, willEat.get(i)[i2]);

            res -= w1 * wn1 + w2 * wn2;
            res += w1 * wp1 + w2 * wp2;
        }

        return res;
    }

    public double calcFoodCost(double x, double y) {

        double res = 0;

        for (int j = 0; j < objects.getFoods().size(); j++) {

            double maxRes = 0;

            for (int i = 0; i < mine.getMe().size(); i++) {

                Fragment me = mine.getMe().get(i);

                double angle = Math.atan2((y - me.getY()), (x - me.getX()));
                angle = angle < 0 ? angle + 2.0 * Math.PI : angle;
                double angleDoubleId = getAngleId(angle);

                int i1 = (int) angleDoubleId;
                int i2 = (i1 + 1) % angles.size();

                double w1 = i1 + 1.0 - angleDoubleId;
                double w2 = angleDoubleId - i1;

                double f1 = eatenFoods.get(i).get(i1)[j] == 0 ? 0 : env.SCORE_FOR_FOOD * Math.pow(env.DECREASE_RATIO, eatenFoods.get(i).get(i1)[j]);
                double f2 = eatenFoods.get(i).get(i2)[j] == 0 ? 0 : env.SCORE_FOR_FOOD * Math.pow(env.DECREASE_RATIO, eatenFoods.get(i).get(i2)[j]);

                double curRes = w1 * f1 + w2 * f2;
                maxRes = Math.max(curRes, maxRes);
            }

            res += maxRes;
        }

        return res;
    }

    private double getAngleId(double angle) {

        if (anglesDiapason == null) {
            return angle * angles.size() / (2.0 * Math.PI);

        } else  if (Double.compare(anglesDiapason[0], anglesDiapason[1]) == 0) {
            return 0;

        } else  {
            double delta = anglesDiapason[1] - anglesDiapason[0];
            double k = (angle - anglesDiapason[0]) * (angles.size() - 1) / delta;
            k = Math.max(Math.min(k, angles.size() - 1), 0);
            return k;
        }
    }

    private double calcVirusCost(double x, double y) {

        // todo implement

        return 0;
    }

    public double calcW1(double x, double y) {

        double res = 0;

        for (int i = 0; i < mine.getMe().size(); i++) {

            Fragment me = mine.getMe().get(i);
            Point c = findPointInNoVision(me.getX(), me.getY(), x, y);
            double w = influenceMap.getTracePotential(c.getX(), c.getY());

            res += w;
        }

        return res / (1.0 * mine.getMe().size());
    }

    private Point findPointInNoVision(double x0, double y0, double x1, double y1) {
        if (mine.inVisionRange(x1, y1)) {
            return new Point(x1, y1);
        }

        double d = Geometry.distance(x0, y0, x1, y1);

        while (d > 1.0) {
            double xm = (x0 + x1) / 2.0;
            double ym = (y0 + y1) / 2.0;

            if (mine.inVisionRange(x1, y1)) {
                x0 = xm;
                y0 = ym;
            } else {
                x1 = xm;
                y1 = ym;
            }
            d = Geometry.distance(x0, y0, x1, y1);
        }

        return new Point((x0 + x1) / 2.0, (y0 + y1) / 2.0);
    }


    public double doPredict(Fragment me, double angle, int deep, int[] eatens, int[] willEaten, int[] willEat) {

        int deepCheckpointId = 0;
        while (deepCheckpointId < env.DEEP_CHECKPOINTS.size() && env.DEEP_CHECKPOINTS.get(deepCheckpointId) <= deep) {
            deepCheckpointId++;
        }
        if (deepCheckpointId >= env.DEEP_CHECKPOINTS.size()) {
            return 0;
        }
        int MAX_N_STRAIGHT_PREDICTS = 4;
        int step = Math.max(1, 1 + (env.DEEP_CHECKPOINTS.get(deepCheckpointId) - deep - 1) / MAX_N_STRAIGHT_PREDICTS);

        final double angleCopy = JavaCore.normalizeAngleInRadians(angle);
        int[] eatensCopy = eatens.clone();
        int[] eatensRes = eatens.clone();
        int[] willEatenRes = {-1};
        int[] willEatRes = {-1};
        Fragment me0 = me;
        double curCost = 0;
        boolean end = false;

        do {
            Fragment pred = me0.predictFragmentByAngle(angleCopy, step, env);

            // из-за этого условия eatenRes не учитывался
            //if (!mine.inVisionRange(pred.getX(), pred.getY())) {
            //    end = true;
            //    break;
            //}

            double deepRatio = Math.pow(env.DECREASE_RATIO, deep);

            // съеденный food
            List<Integer> curEatens = me0.predictEaten(pred.getX(), pred.getY(), tiles, env);
            for (Integer j : curEatens) {
                if (eatensCopy[j] == 0) {
                    curCost += deepRatio;
                    eatensCopy[j] = deep + step;
                    log.info("PreformanceCalculator.java: " + deepRatio);
                }
            }

            int numberOfBurstDangerousOps = 0;
            double meBurstMass = 60;
            for (Fragment op : objects.getOp()) {
                if (op.getMass() > env.MASS_EAT_FACTOR * meBurstMass) {
                    numberOfBurstDangerousOps++;
                }

                Fragment opPred = playerAnalyzer.predictPosition(op, deep + step);
                double dist = Geometry.distance(pred.getX(), pred.getY(), opPred.getX(), opPred.getY());

                if (opPred.getMass() >= env.MASS_EAT_FACTOR * pred.getMass() &&
                        env.isEaten(opPred.getRadius(), pred.getRadius(), dist)) {
                    // he can it me
                    double scoreForEat = mine.getMe().size() == 1 ? env.SCORE_FOR_LAST : env.SCORE_FOR_PLAYER;
                    curCost -= scoreForEat * deepRatio;
                    willEatenRes[0] = deep + step;
                    break;

                } else if (env.MASS_EAT_FACTOR * opPred.getMass() < pred.getMass() &&
                        willEatRes[0] == -1 &&
                        env.isEaten(pred.getRadius(), opPred.getRadius(), dist)) {
                    // i can eat him
                    //log.info("Fragment.java: it happens!" + op.getRadius() + ", " + opVariance + ", " + pred.radius + ", " + dist);
                    curCost += env.SCORE_FOR_PLAYER * deepRatio;
                    willEatRes[0] = deep + step;
                }
            }

            if (willEatenRes[0] != -1) {
                end = true;
                break;
            }

            // viruses
            if (pred.getMass() > env.MIN_SPLIT_MASS && mine.getMe().size() < env.MAX_FRAGS_CNT) {
                double virusCost = 0;
                int uncontrolledTicks = (int)((env.BURST_START_SPEED - env.SPEED_FACTOR / Math.sqrt(meBurstMass))
                        / env.VISCOSITY);
                if (uncontrolledTicks > 10 || numberOfBurstDangerousOps > 0) {
                    virusCost = -env.SCORE_FOR_PLAYER;
                } else {
                    virusCost = env.SCORE_FOR_BURST;
                }
                // может взорваться
                for (Virus virus : objects.getViruses()) {
                    double dist = Geometry.distance(pred.getX(), pred.getY(), virus.getX(), virus.getY());
                    if (env.isEaten(me0.getRadius(), env.VIRUS_RADIUS, dist)) {
                        curCost += virusCost;
                        end = true;
                        break;
                    }
                }
            }
            if (end) {
                break;
            }

            // self collisions
            if (deep < env.DEEP_CHECKPOINTS.get(1) && objects.getDangerOp().size() == 0) {      // todo dangerOp depricated
                // check it
                for (Fragment me1 : mine.getMe()) {
                    if (me1.getfId() == pred.getfId()) {
                        continue;
                    }

                    double px = pred.getX();
                    double py = pred.getY();
                    double dot = Geometry.dotprod(me0.getX() - px, me0.getY() - py, me1.getX() - px, me1.getY() - py);
                    double pm0 = Geometry.distance(px, py, me0.getX(), me0.getY());
                    double pm1 = Geometry.distance(px, py, me1.getX(), me1.getY());
                    double cos = dot / (pm0 * pm1);

                    if (cos < -0.8 && pm1 < me0.getRadius() + pred.getRadius()) {   // todo magic constant
                        end = true;
                        break;
                    }
                }
            }
            if (end) {
                break;
            }

            me0 = pred;
            deep += step;
        } while (deep < env.DEEP_CHECKPOINTS.get(deepCheckpointId));

        double maxCost = Double.NEGATIVE_INFINITY;

        if (deep < env.MAX_PREDICT_DEEP && !end) {


            List<Double> newAngles = new ArrayList<Double>() {{
                add(angleCopy);
            }};
            if (me0.getSpeed() < env.SPEED_FACTOR / Math.sqrt(me0.getMass())) {
                newAngles.add(angleCopy + Math.PI / 4.0);
                newAngles.add(angleCopy - Math.PI / 4.0);
            }

            for (double newAngle : newAngles) {
                int[] eatensCopy2 = eatensCopy.clone();
                int[] willEatenCopy2 = {-1};
                int[] willEatCopy2 = {-1};
                Fragment pred = me0.predictFragmentByAngle(newAngle, env);

                double cost = doPredict(pred, newAngle, deep, eatensCopy2, willEatenCopy2, willEatCopy2);

                if (cost > maxCost) {
                    maxCost = cost;
                    willEatenRes[0] = willEatenCopy2[0];
                    willEatRes[0] = willEatCopy2[0];
                    eatensRes = eatensCopy2.clone();
                }
            }

            maxCost += curCost;

        } else {
            maxCost = curCost;
        }

        willEaten[0] = willEatenRes[0];
        willEat[0] = willEatRes[0];
        System.arraycopy(eatensRes, 0, eatens, 0, eatensRes.length);

        return maxCost;
    }
}
