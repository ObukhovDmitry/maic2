package algo.processors;

import algo.PerformanceCalculator;
import geom.Point;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import algo.PlayerAnalyzer;
import geom.Geometry;
import javafx.util.Pair;
import model.GlobalParameters;
import model.mid_level.Fragment;
import model.top_level.Mine;
import model.top_level.Objects;
import utils.Utils;
import utils.logger.Logger;
import utils.logger.LoggerFactory;
import utils.visualization.Visualizator;
import utils.visualization.VisualizatorFactory;

import java.util.ArrayList;
import java.util.List;

public class AttackProcessor implements IProcessor {

    private static final Logger log = LoggerFactory.getLogger();
    private static final Visualizator vis = VisualizatorFactory.getVisualizator();

    private GlobalParameters env;
    private Objects objects;
    private Mine mine;
    private InfluenceMap influenceMap;
    private Tiles tiles;
    private PlayerAnalyzer playerAnalyzer;

    private List<Pair<Fragment, Fragment>> targets;
    private double performance;
    private Point command;

    public AttackProcessor(GlobalParameters env, Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {
        this.env = env;
        this.objects = objects;
        this.mine = mine;
        this.influenceMap = influenceMap;
        this.tiles = tiles;
        this.playerAnalyzer = playerAnalyzer;
    }

    public List<Pair<Fragment, Fragment>> getTargets() {
        return targets;
    }

    @Override
    public double getPerformance() {
        return performance;
    }

    @Override
    public Point getCommand() {
        return command;
    }

    @Override
    public void preprocess() {

        int[][] attackAbilityDegree = new int[mine.getMe().size()][objects.getOp().size()];

        for (int i = 0; i < mine.getMe().size(); i++) {
            for (int j = 0; j < objects.getOp().size(); j++) {
                attackAbilityDegree[i][j] = calcAttackAbilityDegree(
                        mine.getMe().get(i),
                        objects.getOp().get(j),
                        objects.getOp());
            }
        }

        targets = new ArrayList<>();

        for (int j = 0; j < objects.getOp().size(); j++) {
            List<Fragment> attackers = new ArrayList<>();
            for (int i = 0; i < mine.getMe().size(); i++) {

                if (attackAbilityDegree[i][j] == -1) {
                    attackers.clear();
                    break;
                }

                if (attackAbilityDegree[i][j] == 1) {
                    attackers.add(mine.getMe().get(i));
                }
            }
            if (attackers.size() > 0) {
                Fragment op = objects.getOp().get(j);
                attackers.forEach(f -> targets.add(new Pair<>(f, op)));
            }
        }
    }

    @Override
    public void process() {

        // выбираем цель для атаки и атакующего

        Fragment predator = null;
        double prayX = 0;
        double prayY = 0;
        int minTicks = Integer.MAX_VALUE;

        for (Pair<Fragment, Fragment> pair: targets) {
            Fragment me = pair.getKey();
            Fragment op = pair.getValue();

            for (int dt = 0; dt < Math.min(minTicks, env.MAX_OVERTAKE_TICKS); dt++) {
                Fragment opStar = playerAnalyzer.predictPosition(op, dt);
                opStar.setRadius(env.mass2radius(opStar.getMass()));
                int ticksToOvertake = me.calcTicksToOvertake(opStar.getX(), opStar.getY(), env);
                if (ticksToOvertake < minTicks) {
                    minTicks = ticksToOvertake;
                    predator = me;
                    prayX = opStar.getX();
                    prayY = opStar.getY();
                    break;
                }
            }
        }

        // выбираем лучшую точку на атакующем направлении, с учетом всего игрового мира

        List<Point> possibleTargets = Utils.getPointsOnDirection(predator.getX(), predator.getY(), prayX, prayY, env);
        double[] anglesDiapason = Utils.prepareAnglesForCostProcessor(mine, possibleTargets);

        possibleTargets.forEach(vis::drawPoint);

        PerformanceCalculator performanceCalculator = new PerformanceCalculator(env, objects, mine, influenceMap, tiles, playerAnalyzer);
        performanceCalculator.setAnglesDiapason(anglesDiapason);
        performanceCalculator.fit();

        performance = Double.NEGATIVE_INFINITY;

        for (Point p: possibleTargets) {
            double cost = performanceCalculator.calcCost(p);
            if (performance < cost) {
                performance = cost;
                command = p;
            }
        }

        //log.info("AttackProcessor.java: targets count=" + possibleTargets.size() + ", perf=" + performance);
    }


    private int calcAttackAbilityDegree(Fragment me, Fragment op, List<Fragment> ops) {

        double c = env.MASS_EAT_FACTOR;

        // op может съесть
        if (op.getMass() > c * me.getMass()) {
            return -1;
        }

        // рядом с op есть тот, кто может съесть
        double distOpToMe = Geometry.distance(op.getX(), op.getY(), me.getX(), me.getY());
        for (Fragment op0: ops) {
            if (op0.getMass() < c * me.getMass()) {
                continue;
            }
            double distOpToOp0 = Geometry.distance(op.getX(), op.getY(), op0.getX(), op0.getY());
            if (distOpToOp0 < distOpToMe) {
                return -1;
            }
        }

        // op нельзя съесть
        if (me.getMass() <= c * op.getMass()) {
            return 0;
        }

        // op умеет слишком хорошо уходить
        if (playerAnalyzer.canDodge(op)) {
            return 0;
        }

        // op силшком далеко
        int ticksToOvertake = me.calcTicksToOvertake(op.getX(), op.getY(), env);
        if (ticksToOvertake >= env.MAX_OVERTAKE_TICKS) {
            return 0;
        }

        // ну пути к op есть другой фрагмент
        double midX = (op.getX() + me.getX()) / 2.0;
        double midY = (op.getY() + me.getY()) / 2.0;
        double wayZoneRadius = distOpToMe / 2.0;
        for (Fragment op0: ops) {
            double distMidToOp0 = Geometry.distance(midX, midY, op0.getX(), op0.getY());
            if (distMidToOp0 < wayZoneRadius) {
                return 0;
            }
        }

        return 1;
    }

}
