package algo.processors;

import geom.Point;

public interface IProcessor {

    double getPerformance();

    Point getCommand();

    void preprocess();

    void process();
}
