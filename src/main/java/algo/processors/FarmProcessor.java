package algo.processors;

import algo.PerformanceCalculator;
import algo.PlayerAnalyzer;
import geom.Point;
import geom.Vector;
import javafx.util.Pair;
import model.GlobalParameters;
import model.mid_level.Food;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import model.mid_level.Fragment;
import model.top_level.Mine;
import model.top_level.Objects;
import utils.logger.Logger;
import utils.logger.LoggerFactory;
import utils.visualization.Visualizator;
import utils.visualization.VisualizatorFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class FarmProcessor implements IProcessor {

    private static final Logger log = LoggerFactory.getLogger();
    private static final Visualizator vis = VisualizatorFactory.getVisualizator();

    private static final int N_BOUNDARY_CHECK_POINTS = 32;
    private static final int N_INNER_CHECK_POINTS = 32;
    private static final double PENALTY_DIR_CHANGE = 4e-3;

    private GlobalParameters env;
    private Objects objects;
    private Mine mine;
    private InfluenceMap influenceMap;
    private Tiles tiles;
    private PlayerAnalyzer playerAnalyzer;

    private PerformanceCalculator performanceCalculator;
    private double performance;
    private Point command;


    public FarmProcessor(GlobalParameters env, Objects objects, Mine mine, InfluenceMap influenceMap, Tiles tiles, PlayerAnalyzer playerAnalyzer) {
        this.env = env;
        this.objects = objects;
        this.mine = mine;
        this.influenceMap = influenceMap;
        this.tiles = tiles;
        this.playerAnalyzer = playerAnalyzer;
    }

    @Override
    public double getPerformance() {
        return performance;
    }

    @Override
    public Point getCommand() {
        return command;
    }

    @Override
    public void preprocess() {
        performanceCalculator = new PerformanceCalculator(env, objects, mine, influenceMap, tiles, playerAnalyzer);
        performanceCalculator.fit();
    }

    @Override
    public void process() {

        // выбираем точки, которые будут использоваться в качестве таргета
        List<Point> checkpoints = getTargetPoints();
        checkpoints.forEach(vis::drawPoint);

        // выбираем среди них самую лучшую
        performance = Double.NEGATIVE_INFINITY;
        double maxGlobalCost = Double.NEGATIVE_INFINITY;
        double delta = 1e-3;

        List<Vector> currVect = mine.getMe().stream()
                .map(me0 -> new Vector(mine.getCommandX() - me0.getX(), mine.getCommandY() - me0.getY()))
                .peek(Vector::normalize)
                .collect(Collectors.toList());

        double tmp_food = 0;
        double tmp_op = 0;
        double tmp_w1 = 0;

        for (Point p: checkpoints) {

            List<Vector> newVect = mine.getMe().stream()
                    .map(me0 -> new Vector(p.getX() - me0.getX(), p.getY() - me0.getY()))
                    .peek(Vector::normalize)
                    .collect(Collectors.toList());
            double directionDifference = IntStream.range(0, currVect.size())
                    .mapToDouble(l -> currVect.get(l).dot(newVect.get(l)))
                    .map(t -> (1.0 - t) / 2.0)
                    .map(t -> t * t)
                    .sum();
            directionDifference = Math.sqrt(directionDifference);
            directionDifference *= PENALTY_DIR_CHANGE;


            double cost = performanceCalculator.calcCost(p) - directionDifference;
            double globalCost = influenceMap.getFoodPotential(p.getX(), p.getY());

            if ((performance + delta < cost) ||
                    (performance < cost + delta && maxGlobalCost < globalCost)) {
                performance = cost;
                command = p;
                tmp_food = performanceCalculator.calcFoodCost(p.getX(), p.getY());
                tmp_op = performanceCalculator.calcOpCost(p.getX(), p.getY());
                tmp_w1 = performanceCalculator.calcW1(p.getX(), p.getY());
            }
        }

        log.info("FarmProcessor.java: performance=" + performance);
        log.info("FarmProcessor.java: food=" + tmp_food);
        log.info("FarmProcessor.java: op=" + tmp_op);
        log.info("FarmProcessor.java: w1=" + tmp_w1);
        log.info("FarmProcessor.java: command.x=" + command.getX() + ", command.y=" + command.getY());
    }

    private List<Point> getTargetPoints() {

        List<Point> checkpoints = new ArrayList<>(N_BOUNDARY_CHECK_POINTS);

        double mX = mine.getMe().stream().mapToDouble(Fragment::getX).sum() / mine.getMe().size();
        double mY = mine.getMe().stream().mapToDouble(Fragment::getY).sum() / mine.getMe().size();
        double baseAngle = Math.atan2(mine.getCommandY() - mY, mine.getCommandX() - mX);

        // Точки на границе карты
        for (int i = 0; i < N_BOUNDARY_CHECK_POINTS; i++) {

            // напрвление
            double angle = baseAngle + 2.0 * Math.PI * i / (1.0 * N_BOUNDARY_CHECK_POINTS);
            angle = angle > Math.PI ? angle - 2.0 * Math.PI : angle;

            double nx = Math.cos(angle);
            double ny = Math.sin(angle);

            // точка пересечения с границей
            double kx = Double.compare(nx, 0) == 0 ? Double.POSITIVE_INFINITY : nx > 0 ? (env.GAME_WIDTH  - mX) / nx : -mX / nx;
            double ky = Double.compare(ny, 0) == 0 ? Double.POSITIVE_INFINITY : ny > 0 ? (env.GAME_HEIGHT - mY) / ny : -mY / ny;
            double k = Math.min(kx, ky);

            double bx = mX + k * nx;
            double by = mY + k * ny;

            checkpoints.add(new Point(bx, by));
        }

        // Точки с максимальным значение глобала

        // Список всех тайлов с их весами
        int numberOfTiles = Tiles.N_TILES * Tiles.N_TILES;
        List<Pair<Pair<Integer, Integer>, Double>> allTiles = new ArrayList<>(numberOfTiles);
        for (int i = 0; i < Tiles.N_TILES; i++) {
            for (int j = 0; j < Tiles.N_TILES; j++) {
                Pair<Integer, Integer> indexes = new Pair<>(i, j);
                double x = tiles.getCenterX(i, j);
                double y = tiles.getCenterY(i, j);
                double w = influenceMap.getFoodPotential(x, y);
                allTiles.add(new Pair<>(indexes, w));
            }
        }

        // Сортируем его
        allTiles.sort(Comparator.comparing(Pair::getValue));
        Collections.reverse(allTiles);

        // Берем из него топ nTopTiles
        List<Point> topTiles = allTiles.stream()
                .limit(N_INNER_CHECK_POINTS)
                .map(p -> {
                    int i = p.getKey().getKey();
                    int j = p.getKey().getValue();
                    double x = tiles.getCenterX(i, j);
                    double y = tiles.getCenterY(i, j);
                    return new Point(x, y);
                })
                .collect(Collectors.toList());
        checkpoints.addAll(topTiles);

        return checkpoints;
    }
}
