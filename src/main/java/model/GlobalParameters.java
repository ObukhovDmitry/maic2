package model;

import model.mid_level.Fragment;
import model.top_level.Mine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//todo мб tick index логично засунуть сюда
public class GlobalParameters {

    // Изменяемые
    public final int GAME_TICKS;
    public final int GAME_HEIGHT;
    public final int GAME_WIDTH;
    public final double INERTION_FACTOR;
    public final double VISCOSITY;
    public final double SPEED_FACTOR; // speed = SF / sqrt(mass)
    public final double FOOD_MASS;
    public final double VIRUS_RADIUS;
    public final double VIRUS_SPLIT_MASS;
    public final int MAX_FRAGS_CNT;
    public final int TICKS_TIL_FUSION;

    // Неизменяемые
    public final int START_FOOD_SETS = 4;
    public final int ADD_FOOD_SETS = 2;
    public final int START_VIRUS_SETS = 1;
    public final int ADD_VIRUS_SETS = 1;
    public final double FOOD_RADIUS = 2.5;
    public final int ADD_FOOD_DELAY = 40;
    public final double VIRUS_MASS = 40.0;
    public final int ADD_VIRUS_DELAY = 1200;
    public final double RADIUS_FACTOR = 2.0; // radius = RF * sqrt(mass)
    public final double MASS_EAT_FACTOR = 1.20; // mass > food.mass * MEF
    public final double DIAM_EAT_FACTOR = 2./3.; // (radius - food.radius * REF) > dist
    public final double RAD_HURT_FACTOR = 0.66; // (radius * RHF + player.radius) > dist
    public final double BURST_START_SPEED = 8.0;
    public final double BURST_ANGLE_SPECTRUM = 3.14159265359; // angle - BAM / 2 + I*BAM / frags_cnt
    public final double MIN_SPLIT_MASS = 120.0; // MSM < mass
    public final double SPLIT_START_SPEED = 9.0;
    public final double MIN_EJECT_MASS = 40.0;
    public final double EJECT_START_SPEED = 8.0;
    public final double EJECT_RADIUS = 4.0;
    public final double EJECT_MASS = 15.0;
    public final double VIRUS_SPLIT_SPEED = 8.0;
    public final double MIN_SHRINK_MASS = 100;
    public final double SHRINK_FACTOR = 0.01; // (-1) * (mass - MSM) * SF
    public final int SHRINK_EVERY_TICK = 50;
    public final double BURST_BONUS = 5.0; // mass += BB

    public final double VISION_SHIFT = 10.0;
    public final double VISION_SINGLE_COEF = 4.0;
    public final double VISION_MULTI_COEF = 2.5;

    public final int SCORE_FOR_FOOD = 1;
    public final int SCORE_FOR_PLAYER = 10;
    public final int SCORE_FOR_LAST = 100;
    public final int SCORE_FOR_BURST = 2;
    public final int SCORE_FOR_EJECT = 0;

    // my constants and globals
    public final double DECREASE_RATIO = 0.995;
    public final int MAX_OVERTAKE_TICKS = 30;

    public List<Integer> DEEP_CHECKPOINTS;
    public int MAX_PREDICT_DEEP;
    public int N_DIRECTIONS;

    private final int[][] DEEP_CHECKPOINTS_ARRAYS = {
            {3, 6, 10, 15, 21},
            {4, 8, 13, 19, 26},
            {5, 10, 16, 23, 31},
            {6, 12, 19, 27, 36}};

    public GlobalParameters(
            int GAME_TICKS,
            int GAME_HEIGHT,
            int GAME_WIDTH,
            double INERTION_FACTOR,
            double VISCOSITY,
            double SPEED_FACTOR,
            double FOOD_MASS,
            double VIRUS_RADIUS,
            double VIRUS_SPLIT_MASS,
            int MAX_FRAGS_CNT,
            int TICKS_TIL_FUSION) {

        this.GAME_TICKS = GAME_TICKS;
        this.GAME_HEIGHT = GAME_HEIGHT;
        this.GAME_WIDTH = GAME_WIDTH;
        this.INERTION_FACTOR = INERTION_FACTOR;
        this.VISCOSITY = VISCOSITY;
        this.SPEED_FACTOR = SPEED_FACTOR;
        this.FOOD_MASS = FOOD_MASS;
        this.VIRUS_RADIUS = VIRUS_RADIUS;
        this.VIRUS_SPLIT_MASS = VIRUS_SPLIT_MASS;
        this.MAX_FRAGS_CNT = MAX_FRAGS_CNT;
        this.TICKS_TIL_FUSION = TICKS_TIL_FUSION;
        this.MAX_PREDICT_DEEP = SPEED_FACTOR < 50 ? 28 : SPEED_FACTOR < 75 ? 21 : 15;
    }

    public void update(Mine mine) {
        double mass = mine.getMe().stream().mapToDouble(Fragment::getMass).min().orElse(0);
        updateDeepCheckPoints(mine.getMe().size(), mass);
        updateNumberOfFarmControllerDirections(mine.getMe().size());
    }

    private void updateNumberOfFarmControllerDirections(int fragsCnt) {
        N_DIRECTIONS = fragsCnt < 7 ? 8 : fragsCnt < 13 ? 6 : 4;
    }

    public boolean isEaten(double predatorRadius, double preyRadius, double distance) {
        return distance - preyRadius + (preyRadius * 2) * DIAM_EAT_FACTOR < predatorRadius;
    }

    public double mass2radius(double mass) {
        return 2.0 * Math.sqrt(mass);
    }

    public void updateDeepCheckPoints(int fragsCnt, double mass) {

        double T = calcTicksToOvertakeVisionRange(fragsCnt, mass);
        double alpha = 1.2; // todo magic constant
        double alpha4 = 1 + alpha + alpha*alpha + alpha*alpha*alpha;
        double t0 = T / alpha4;
        double t1 = t0 * alpha + t0;
        double t2 = t1 * alpha + t0;
        double t3 = t2 * alpha + t0;
        double t4 = t3 * alpha + t0;
        int n0 = Math.max((int) t0, 1);
        int n1 = Math.max((int) t1, n0 + 1);
        int n2 = Math.max((int) t2, n1 + 1);
        int n3 = Math.max((int) t3, n2 + 1);
        int n4 = Math.max((int) t4, n3 + 1);

        DEEP_CHECKPOINTS = Arrays.asList(n0, n1, n2, n3, n4);
        MAX_PREDICT_DEEP = DEEP_CHECKPOINTS.get(DEEP_CHECKPOINTS.size() - 1);
    }

    private double calcTicksToOvertakeVisionRange(int fragsCnt, double mass) {

        double visionCoefficient = fragsCnt == 1 ? VISION_SINGLE_COEF : VISION_MULTI_COEF * Math.sqrt(fragsCnt);
        double S = visionCoefficient * RADIUS_FACTOR * Math.sqrt(mass);
        double maxSpeed = SPEED_FACTOR / Math.sqrt(mass);
        double alpha = INERTION_FACTOR / mass;
        double alpha1 = 1.0 - alpha;

        int l = 5;
        int r = 200;    // max tick for predict

        while (l < r) {
            int k = (l + r) / 2;

            double alpha2 = 1.0 - Math.pow(alpha1, k);
            double alpha3 = alpha1 * alpha2 / alpha;
            double Sk = (k - alpha3) * maxSpeed;


            if (Sk < S) {
                l = k + 1;
            } else {
                r = k;
            }
        }

        return l;
    }
}
