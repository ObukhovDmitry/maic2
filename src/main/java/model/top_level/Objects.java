package model.top_level;

import model.mid_level.Food;
import model.GlobalParameters;
import model.mid_level.Fragment;
import model.mid_level.Virus;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Objects {

    private static final Logger log = LoggerFactory.getLogger();

    private int tickIndex;
    private boolean changed;

    private List<Food> missingFood;
    private List<Food> foods;
    private List<Virus> viruses;
    private List<Fragment> dangerOp;
    private List<Fragment> op;


    public Objects() {
        tickIndex = 0;
        missingFood = new ArrayList<>(128);
        foods = new ArrayList<>(256);
        viruses = new ArrayList<>(32);
        op = new ArrayList<>(16);
    }

    public int getTickIndex() {
        return tickIndex;
    }

    public boolean isChanged() {
        return changed;
    }

    public List<Food> getFoods() {
        return foods;
    }

    public List<Virus> getViruses() {
        return viruses;
    }

    public List<Food> getEatenFoods() {
        return missingFood;
    }

    public List<Fragment> getOp() {
        return op;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public void incrementTickIndex() {
        tickIndex++;
    }

    public void setViruses(List<Virus> viruses) {
        this.viruses = viruses;
    }

    public void updateOp(List<Fragment> opUpdate, Mine mine, GlobalParameters env) {

        changed |= opUpdate.size() > 0;

        opUpdate.forEach(f -> {
            for (Fragment op0: op) {
                if (op0.getpId() == f.getpId() && op0.getfId() == f.getfId()) {
                    f.setSx(f.getX() - op0.getX());
                    f.setSy(f.getY() - op0.getY());
                    break;
                }
            }
        });

        this.op = opUpdate;

        double mass = mine.getMe().stream()
                .mapToDouble(Fragment::getMass)
                .min()
                .orElse(0);

        this.dangerOp = opUpdate.stream()
                .filter(f -> f.getMass() + env.FOOD_MASS >= env.MASS_EAT_FACTOR * mass)
                .collect(Collectors.toList());

        //log.info("Objects.java: mass=" + mass);
        //dangerOp.forEach(f -> log.info("Objects.java: " + f.getMass()));
    }

    public void updateFoods(Mine mine, List<Food> foodsUpdate) {

        List<Food> newFoods = foodsUpdate.stream()
                .filter(x -> foods.stream().noneMatch(y -> y.isSame(x)))
                .collect(Collectors.toList());

        missingFood = foods.stream()
                .filter(x -> foodsUpdate.stream().noneMatch(y -> y.isSame(x)))
                .filter(x -> mine.inVisionRange(x.getX(), x.getY()))
                .collect(Collectors.toList());

        changed |= newFoods.size() > 0 || missingFood.size() > 0;

        foods = foodsUpdate;
    }

    public List<Fragment> getDangerOp() {
        return dangerOp;
    }

    public Objects predictNextTick() {

        // todo implement

        return this;
    }
}
