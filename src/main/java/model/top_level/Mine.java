package model.top_level;

import geom.Geometry;
import model.GlobalParameters;
import model.mid_level.Fragment;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Mine {

    private GlobalParameters env;
    private List<Fragment> me;
    private double commandX;
    private double commandY;
    private boolean split;

    public Mine(GlobalParameters env) {
        this.env = env;
        me = new ArrayList<>(16);
    }

    public double getCommandX() {
        return commandX;
    }

    public double getCommandY() {
        return commandY;
    }

    public boolean isSplit() {
        return split;
    }

    public void setCommandX(double commandX) {
        this.commandX = commandX;
    }

    public void setCommandY(double commandY) {
        this.commandY = commandY;
    }

    public void setSplit(boolean split) {
        this.split = split;
    }

    public List<Fragment> getMe() {
        return me;
    }

    public void setMe(List<Fragment> newMe) {
        me = newMe;
        me.sort(new Comparator<Fragment>() {
            @Override
            public int compare(Fragment o1, Fragment o2) {
                int cmp1 = -Double.compare(o1.getMass(), o2.getMass());
                return cmp1 != 0 ? cmp1 : -Integer.compare(o1.getfId(), o2.getfId()); // todo test it
            }
        });
    }


    public boolean isSplitPossible() {

        boolean splitPossible = false;

        if (me.size() >= env.MAX_FRAGS_CNT) {
            return false;
        }

        for (Fragment me0: me) {
            if (me0.getMass() >= env.MIN_SPLIT_MASS) {
                splitPossible = true;
                break;
            }
        }

        return splitPossible;
    }

    public boolean isFusePossible() {

        boolean fusePossible = false;

        if (me.size() <= 1) {
            return false;
        }

        for (Fragment me0: me) {
            if (me0.getTtf() > 0) {
                continue;
            }

            for (Fragment me1: me) {
                if (me0 == me1 || me1.getTtf() > 0) {
                    continue;
                }
                double dist = Geometry.distance(me0.getX(), me0.getY(), me1.getX(), me1.getY());
                fusePossible |= dist < me0.getRadius() + me1.getRadius();
            }
        }

        return fusePossible;
    }


    public boolean inVisionRange(double x, double y) {

        boolean inVision = false;

        if (me.size() == 1) {
            double sx = me.get(0).getSx();
            double sy = me.get(0).getSy();
            double s = Math.sqrt(sx * sx + sy * sy);

            double visionCenterX = me.get(0).getX() + env.VISION_SHIFT * sx / s ;
            double visionCenterY = me.get(0).getY() + env.VISION_SHIFT * sy / s ;
            double visionRadius = env.VISION_SINGLE_COEF * me.get(0).getRadius();

            inVision = Geometry.distance(x, y, visionCenterX, visionCenterY) < visionRadius;

        } else if (me.size() > 1) {
            double visionCoef = env.VISION_MULTI_COEF * Math.sqrt(me.size());
            for (Fragment me0: me) {
                double sx = me0.getSx();
                double sy = me0.getSy();
                double s = Math.sqrt(sx * sx + sy * sy);

                double visionCenterX = me0.getX() + env.VISION_SHIFT * sx / s ;
                double visionCenterY = me0.getY() + env.VISION_SHIFT * sy / s ;
                double visionRadius = visionCoef * me0.getRadius();

                if (Geometry.distance(x, y, visionCenterX, visionCenterY) < visionRadius) {
                    inVision = true;
                    break;
                }
            }
        }

        return inVision;
    }

    public Mine predictMineSplit(double x, double y) {

        List<Fragment> meDivided = new ArrayList<>();
        int countOfMe = me.size();

        for (Fragment me0: me) {
            if (me0.getMass() >= env.MIN_SPLIT_MASS && countOfMe < env.MAX_FRAGS_CNT) {
                Fragment pred =  me0.predictFragment(x, y, env);

                double newMass = pred.getMass() / 2.0;
                double newRadius = env.mass2radius(newMass);
                double oldS = Math.sqrt(pred.getSx() * pred.getSx() + pred.getSy() * pred.getSy());
                double newSX = env.SPLIT_START_SPEED * pred.getSx() / oldS;
                double newSY = env.SPLIT_START_SPEED * pred.getSy() / oldS;

                pred.setMass(newMass);
                pred.setRadius(newRadius);
                pred.setTtf(env.TICKS_TIL_FUSION);

                Fragment meNew = new Fragment(me0.getpId(), 17, pred.getX(), pred.getY(), newMass, newRadius, newSX, newSY, env.TICKS_TIL_FUSION);

                meDivided.add(pred);
                meDivided.add(meNew);

                countOfMe++;

            } else {
                meDivided.add(me0.predictFragment(x, y, env));
            }
        }

        Mine mineDivided = new Mine(env);
        mineDivided.setMe(meDivided);

        return mineDivided;
    }
}
