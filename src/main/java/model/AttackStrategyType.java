package model;

public enum AttackStrategyType {

    CHASE,
    SPLIT,
    FUSE
}
