package model.tiles;

import model.mid_level.Food;

import java.util.ArrayList;
import java.util.List;

public class Tile {

    private List<Food> actualFoods;
    private List<Food> foods;

    public Tile() {
        actualFoods = new ArrayList<>(8);
        foods = new ArrayList<>(16);
    }

    public List<Food> getActualFoods() {
        return actualFoods;
    }

    public List<Food> getFoods() {
        return foods;
    }


    public void prepareToUpdate() {
        actualFoods.forEach(this::addFood);
        actualFoods.clear();
    }

    public void addFood(Food food) {
        if (!contains(food)) {
            foods.add(food);
        }
    }

    public void addActualFood(Food food) {
        actualFoods.add(food);
        removeFood(food);
        addFood(food);
    }

    public void removeFood(Food food) {
        foods.removeIf(f -> f.isSame(food));
    }

    public void removeNotUpdatedFood(int lastUpdate) {
        foods.removeIf(f -> f.getLastUpdate() != lastUpdate);
    }

    public boolean contains(Food food) {
        return foods.stream().anyMatch(f -> f.isSame(food));
    }
}
