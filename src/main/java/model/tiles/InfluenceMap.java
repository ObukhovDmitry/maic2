package model.tiles;

import geom.Geometry;
import model.GlobalParameters;
import model.mid_level.Food;
import model.top_level.Objects;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import static model.tiles.Tiles.N_TILES;

public class InfluenceMap {

    private final Logger log = LoggerFactory.getLogger();

    private GlobalParameters env;
    private Tiles tiles;

    private double[][] w1;
    private double[][] w2;
    private double[][] tracePotential;
    private double[][] foodPotential;


    public InfluenceMap(GlobalParameters env, Tiles tiles) {
        this.env = env;
        this.tiles = tiles;

        w1 = new double[N_TILES][N_TILES];
        w2 = new double[N_TILES][N_TILES];
        tracePotential = new double[N_TILES][N_TILES];
        foodPotential = new double[N_TILES][N_TILES];
    }


    public double getTracePotential(double x, double y) {
        int i = tiles.calcIndexByCoordinate(x, tiles.TILE_WIDTH, env.GAME_WIDTH);
        int j = tiles.calcIndexByCoordinate(y, tiles.TILE_HEIGHT, env.GAME_HEIGHT);

        return tracePotential[i][j];
    }

    public double getFoodPotential(double x, double y) {
        int i = tiles.calcIndexByCoordinate(x, tiles.TILE_WIDTH, env.GAME_WIDTH);
        int j = tiles.calcIndexByCoordinate(y, tiles.TILE_HEIGHT, env.GAME_HEIGHT);

        return foodPotential[i][j];
    }


    public void update(Objects objects) {

        updateW1(objects);
        updateW2(objects);
        updateTracePotential(objects);
        updateFoodPotential(objects);

        //for (int i = 0; i < N_TILES / 2; i++) {
        //    for (int j = 0; j < N_TILES / 2; j++) {
        //        if (foodPotential[i][j] > 0) {
        //            log.info("InfluenceMap.java: foodPotential=" + foodPotential[i][j] + ", x=" + tiles.getCenterX(i, j) + ", y=" + tiles.getCenterY(i, j));
        //        }
        //    }
        //}
    }


    private void updateW1(Objects objects) {
        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {
                w1[i][j] = 4.0 * env.ADD_FOOD_SETS / env.ADD_FOOD_DELAY * (objects.getTickIndex() - tiles.getLastUpdate(i, j)) / (1.0 * N_TILES * N_TILES);
            }
        }
    }

    private void updateW2(Objects objects) {
        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {
                w2[i][j] = tiles.getTile(i, j).getFoods().stream().mapToDouble(f -> Math.pow(env.DECREASE_RATIO, objects.getTickIndex() - f.getLastUpdate())).sum();
            }
        }
    }

    private void updateTracePotential(Objects objects) {
        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {

                int nNeighbors = 8;
                double neighbors = 0;
                for (int i0 = Math.max(0, i - 1); i0 < Math.min(N_TILES, i + 1); i0++) {
                    for (int j0 = Math.max(0, j - 1); j0 < Math.min(N_TILES, j + 1); j0++) {
                        neighbors += w1[i0][j0];
                        //nNeighbors++;
                    }
                }

                tracePotential[i][j] = 0.5 * w1[i][j] + neighbors / (2.0 * nNeighbors);
            }
        }
    }

    private void updateFoodPotential(Objects objects) {

        double zone1 = 30.0;
        double zone2 = 60.0;

        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {

                double x = tiles.getCenterX(i, j);
                double y = tiles.getCenterY(i, j);

                int k = 2;      // todo magic constants
                double res = 0;

                for (int i0 = Math.max(0, i - k); i0 < Math.min(N_TILES, i + k); i0++) {
                    for (int j0 = Math.max(0, j - k); j0 < Math.min(N_TILES, j + k); j0++) {
                        Tile current = tiles.getTile(i0, j0);
                        for (Food food: current.getFoods()) {
                            double distFromTileToFood = Geometry.distance(x, y, food.getX(), food.getY());
                            double weight = (zone2 - distFromTileToFood) / (zone2 - zone1);
                            weight = Math.max(Math.min(weight, 1), 0);

                            res += weight * Math.pow(env.DECREASE_RATIO, objects.getTickIndex() - food.getLastUpdate());
                        }
                    }
                }

                foodPotential[i][j] = res;
            }
        }
    }
}
