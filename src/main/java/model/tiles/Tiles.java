package model.tiles;

import model.GlobalParameters;
import model.mid_level.Food;
import model.top_level.Mine;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

public class Tiles {

    private final Logger log = LoggerFactory.getLogger();

    public static final int N_TILES = 32;
    public final double TILE_WIDTH;
    public final double TILE_HEIGHT;

    private GlobalParameters env;
    private int[][] lastUpdates;
    private Tile[][] tiles;

    public Tiles(GlobalParameters env) {
        this.env = env;
        TILE_WIDTH  = env.GAME_WIDTH / (1.0 * N_TILES);
        TILE_HEIGHT = env.GAME_HEIGHT / (1.0 * N_TILES);

        lastUpdates = new int[N_TILES][N_TILES];
        tiles = new Tile[N_TILES][N_TILES];
        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {
                tiles[i][j] = new Tile();
            }
        }
    }

    public int getLastUpdate(int i, int j) {
        return lastUpdates[i][j];
    }

    public double getCenterX(int i, int j) {
        return TILE_WIDTH * (i + 0.5);
    }

    public double getCenterY(int i, int j) {
        return TILE_HEIGHT * (j + 0.5);
    }

    public int calcIndexByCoordinate(double t, double tileSize, double maxSize) {

        if (t <= maxSize / 2) {
            return (int) (t / tileSize);

        } else  { // if (t > maxSize / 2) {
            int iSym = (int) ((maxSize - t) / tileSize);
            return N_TILES - 1 - iSym;
        }
    }

    public void updateLastUpdate(Mine mine, int tick) {

        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {
                double x0 = TILE_WIDTH * i;
                double y0 = TILE_HEIGHT * j;
                double x1 = TILE_WIDTH * (i + 1);
                double y1 = TILE_HEIGHT * (j + 1);

                boolean inVision = mine.inVisionRange(x0, y0) && mine.inVisionRange(x0, y1) &&
                        mine.inVisionRange(x1, y0) && mine.inVisionRange(x1, y1);

                if (inVision) {
                    lastUpdates[i][j] = tick;
                    tiles[i][j].removeNotUpdatedFood(tick);
                    //log.info(String.format("updated tile: %d %d", i, j));
                }
            }
        }
    }




    public Tile getTile(int i, int j) {
        return tiles[i][j];
    }


    public void prepareToUpdate() {
        for (int i = 0; i < N_TILES; i++) {
            for (int j = 0; j < N_TILES; j++) {
                tiles[i][j].prepareToUpdate();
            }
        }
    }

    public void updateWithActualFood(Food food, int tick) {
        int i = calcIndexByCoordinate(food.getX(), TILE_WIDTH, env.GAME_WIDTH);
        int j = calcIndexByCoordinate(food.getY(), TILE_HEIGHT, env.GAME_HEIGHT);

        //log.info(String.format("Tiles.java: new food %f %f:", food.getX(), food.getY()));
        //log.info(String.format("Tiles.java: actual foods for %d %d:", i, j));
        //tiles[i][j].getFoods().forEach(f -> log.info(String.format("Tiles.java: %f %f", f.getX(), f.getY())));

        if (tiles[i][j].contains(food)) {
            tiles[i][j].addActualFood(food);
            //log.info(String.format("Tiles.java: %d %d contains it", i, j));

        } else {
            int iSym = N_TILES - 1 - i;
            int jSym = N_TILES - 1 - j;
            int tick40 = tick - (tick % env.ADD_FOOD_DELAY);

            tiles[i][j].addActualFood(food);
            tiles[i][jSym].addFood(new Food(food.getX(), env.GAME_HEIGHT - food.getY(), tick40));
            tiles[iSym][j].addFood(new Food(env.GAME_WIDTH - food.getX(), food.getY(), tick40));
            tiles[iSym][jSym].addFood(new Food(env.GAME_WIDTH - food.getX(), env.GAME_HEIGHT - food.getY(), tick40));

            //log.info(String.format("Tiles.java: %d %d new", i, j));
        }
    }

    public void updateWithEatenFood(Food food, int tick) {
        int i = calcIndexByCoordinate(food.getX(), TILE_WIDTH, env.GAME_WIDTH);
        int j = calcIndexByCoordinate(food.getY(), TILE_HEIGHT, env.GAME_HEIGHT);
        tiles[i][j].removeFood(food);
    }
}
