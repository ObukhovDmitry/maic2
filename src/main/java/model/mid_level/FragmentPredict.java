package model.mid_level;

import utils.logger.Logger;
import utils.logger.LoggerFactory;

public class FragmentPredict {

    private static final Logger log = LoggerFactory.getLogger();

    private String id;
    private double x;
    private double y;
    private double mass;
    private double radius;
    private double sx;
    private double sy;
    private int ttf;

    public FragmentPredict(String id, double x, double y, double mass, double radius) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.mass = mass;
        this.radius = radius;
        sx = 0;
        sy = 0;
        ttf = 0;
    }

    public FragmentPredict(String id, double x, double y, double mass, double radius, double sx, double sy) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.mass = mass;
        this.radius = radius;
        this.sx = sx;
        this.sy = sy;
        ttf = 0;
    }

    public String getId() {
        return id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getMass() {
        return mass;
    }

    public double getRadius() {
        return radius;
    }

    public double getSx() {
        return sx;
    }

    public double getSy() {
        return sy;
    }


    public void setSx(double sx) {
        this.sx = sx;
    }

    public void setSy(double sy) {
        this.sy = sy;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }


}
