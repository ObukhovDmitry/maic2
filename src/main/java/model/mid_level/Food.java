package model.mid_level;

import geom.Geometry;

public class Food {

    private int id;
    private int lastUpdate;
    private double x;
    private double y;

    public Food(int id, double x, double y, int lastUpdate) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.lastUpdate = lastUpdate;
    }

    public Food(double x, double y, int lastUpdate) {
        this.id = -1;
        this.x = x;
        this.y = y;
        this.lastUpdate = lastUpdate;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getId() {
        return id;
    }

    public int getLastUpdate() {
        return lastUpdate;
    }


    public void setX(double x) {
        this.x = x;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setLastUpdate(int lastUpdate) {
        this.lastUpdate = lastUpdate;
    }


    public boolean isSame(Food food) {
        return Geometry.distance2(x, y, food.getX(), food.getY()) < 0.5;
    }
}
