package model.mid_level;

import model.tiles.InfluenceMap;
import model.tiles.Tile;
import model.tiles.Tiles;
import geom.Geometry;
import geom.Point;
import model.GlobalParameters;
import model.top_level.Mine;
import model.top_level.Objects;
import utils.JavaCore;
import utils.logger.Logger;
import utils.logger.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Fragment {

    // todo возможно стоит ввести параметр predictedMass для массы противника

    private static final Logger log = LoggerFactory.getLogger();

    private int pId;
    private int fId;
    private double x;
    private double y;
    private double mass;
    private double radius;
    private double sx;
    private double sy;
    private int ttf;

    public Fragment(int pId, int fId, double x, double y, double mass, double radius) {
        this.pId = pId;
        this.fId = fId;
        this.x = x;
        this.y = y;
        this.mass = mass;
        this.radius = radius;
        ttf = 0;
        sx = 0;
        sy = 0;
        ttf = 0;
    }

    public Fragment(int pId, int fId, double x, double y, double mass, double radius, double sx, double sy, int ttf) {
        this.pId = pId;
        this.fId = fId;
        this.x = x;
        this.y = y;
        this.mass = mass;
        this.radius = radius;
        this.sx = sx;
        this.sy = sy;
        this.ttf = Math.max(ttf, 0);
    }

    public int getpId() {
        return pId;
    }

    public int getfId() {
        return fId;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getMass() {
        return mass;
    }

    public double getRadius() {
        return radius;
    }

    public double getSx() {
        return sx;
    }

    public double getSy() {
        return sy;
    }

    public double getSpeed() {
        return Math.sqrt(sx * sx + sy * sy);
    }

    public int getTtf() {
        return ttf;
    }


    public void setSx(double sx) {
        this.sx = sx;
    }

    public void setSy(double sy) {
        this.sy = sy;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setTtf(int ttf) {
        this.ttf = ttf;
    }


    public Point predictPosition(double toX, double toY, GlobalParameters env) {

        double dx = toX - x;
        double dy = toY - y;
        double dist = Math.sqrt(dx * dx + dy * dy);
        double nx = (dist > 0) ? dx / dist : 0;
        double ny = (dist > 0) ? dy / dist : 0;

        return predictPositionByDirection(nx, ny, env);
    }

    public Point predictPositionByDirection(double nx, double ny, GlobalParameters env) {

        double sCoef = env.INERTION_FACTOR / mass;
        double maxSpeed = env.SPEED_FACTOR / Math.sqrt(mass);

        double sxPred = sx + (nx * maxSpeed - sx) * sCoef;
        double syPred = sy + (ny * maxSpeed - sy) * sCoef;
        double s2 = Math.sqrt(sxPred * sxPred + syPred * syPred);
        if (s2 > maxSpeed) {
            sxPred *= maxSpeed / s2;
            syPred *= maxSpeed / s2;
        }

        double xPred = x + sxPred;
        double yPred = y + syPred;
        if (xPred - radius < 0) { xPred = radius; }
        if (yPred - radius < 0) { yPred = radius; }
        if (xPred + radius > env.GAME_WIDTH) { xPred = env.GAME_WIDTH - radius; }
        if (yPred + radius > env.GAME_HEIGHT) { yPred = env.GAME_HEIGHT - radius; }

        return new Point(xPred, yPred);
    }

    public Fragment predictFragment(double toX, double toY, GlobalParameters env) {

        double dx = toX - x;
        double dy = toY - y;
        double dist = Math.sqrt(dx * dx + dy * dy);
        double nx = (dist > 0) ? dx / dist : 0;
        double ny = (dist > 0) ? dy / dist : 0;

        return predictFragmentByDirection(nx, ny, env);
    }

    public Fragment predictFragmentByAngle(double angle, GlobalParameters env) {

        double nx = Math.cos(angle);
        double ny = Math.sin(angle);

        return predictFragmentByDirection(nx, ny, env);
    }

    public Fragment predictFragmentByDirection(double nx, double ny, GlobalParameters env) {

        double sCoef = env.INERTION_FACTOR / mass;
        double speed = Math.sqrt(sx * sx + sy * sy);
        double maxSpeed = env.SPEED_FACTOR / Math.sqrt(mass);

        if (speed > maxSpeed) {
            double speedPred = speed - env.VISCOSITY;

            double xPred = x + sx;
            double yPred = y + sy;
            double sxPred = sx * speedPred / speed;
            double syPred = sy * speedPred / speed;

            if (xPred - radius < 0) { xPred = radius; sxPred = 0; }
            if (yPred - radius < 0) { yPred = radius; syPred = 0; }
            if (xPred + radius > env.GAME_WIDTH) { xPred = env.GAME_WIDTH - radius; sxPred = 0; }
            if (yPred + radius > env.GAME_HEIGHT) { yPred = env.GAME_HEIGHT - radius; syPred = 0; }

            return new Fragment(pId, fId, xPred, yPred, mass, radius, sxPred, syPred, ttf - 1);

        } else {

            double sxPred = sx + (nx * maxSpeed - sx) * sCoef;
            double syPred = sy + (ny * maxSpeed - sy) * sCoef;
            double s2 = Math.sqrt(sxPred * sxPred + syPred * syPred);
            if (s2 > maxSpeed) {
                sxPred *= maxSpeed / s2;
                syPred *= maxSpeed / s2;
            }

            double xPred = x + sxPred;
            double yPred = y + syPred;

            if (xPred - radius < 0) { xPred = radius; sxPred = 0; }
            if (yPred - radius < 0) { yPred = radius; syPred = 0; }
            if (xPred + radius > env.GAME_WIDTH) { xPred = env.GAME_WIDTH - radius; sxPred = 0; }
            if (yPred + radius > env.GAME_HEIGHT) { yPred = env.GAME_HEIGHT - radius; syPred = 0; }

            return new Fragment(pId, fId, xPred, yPred, mass, radius, sxPred, syPred, ttf - 1);
        }
    }


    public List<Integer> predictEaten(double toX, double toY, Tiles tiles, GlobalParameters env) {

        List<Integer> eatens = new ArrayList<>();

        double x0 = Math.min(x, toX) - radius;
        double x1 = Math.max(x, toX) + radius;
        double y0 = Math.min(y, toY) - radius;
        double y1 = Math.max(y, toY) + radius;

        int i0 = tiles.calcIndexByCoordinate(x0, tiles.TILE_WIDTH, env.GAME_WIDTH);
        int i1 = tiles.calcIndexByCoordinate(x1, tiles.TILE_WIDTH, env.GAME_WIDTH);
        int j0 = tiles.calcIndexByCoordinate(y0, tiles.TILE_HEIGHT, env.GAME_HEIGHT);
        int j1 = tiles.calcIndexByCoordinate(y1, tiles.TILE_HEIGHT, env.GAME_HEIGHT);

        for (int i = i0; i <= i1; i++) {
            for (int j = j0; j <= j1; j++) {
                Tile current = tiles.getTile(i, j);
                for (Food food: current.getActualFoods()) {
                    double distToFood = Geometry.distanceFromPointToSegment(food.getX(), food.getY(),
                            x, y, toX, toY);
                    if (env.isEaten(radius, env.FOOD_RADIUS, distToFood)) {
                        eatens.add(food.getId());
                    }
                }
            }
        }

        return eatens;
    }

    public Fragment predictFragmentByAngle(double angle, int nTicks, GlobalParameters env) {

        // профилактика, рекурсия все-таки
        if (nTicks <= 0) {
            return new Fragment(pId, fId, x, y, mass, radius, sx, sy, ttf);
        }

        double speed = Math.sqrt(sx * sx + sy * sy);
        double maxSpeed = env.SPEED_FACTOR / Math.sqrt(mass);

        // special case if fragment is fast
        if (speed > maxSpeed) {
            // едет по прямой пока скорость не станет меньше maxSpeed
            int nStraightTicks = (int) ((speed - maxSpeed) / env.VISCOSITY) + 1;
            nStraightTicks = Math.min(nStraightTicks, nTicks);

            double nx = sx / speed;
            double ny = sy / speed;

            double speedPred = speed - nStraightTicks * env.VISCOSITY;
            double sxPred = nx * speedPred;
            double syPred = ny * speedPred;

            double xPred = x + nStraightTicks * sx - nx * env.VISCOSITY * nStraightTicks * (nStraightTicks + 1) / 2.0;
            double yPred = y + nStraightTicks * sy - ny * env.VISCOSITY * nStraightTicks * (nStraightTicks + 1) / 2.0;

            if (xPred - radius < 0) { xPred = radius; sxPred = 0; }
            if (yPred - radius < 0) { yPred = radius; syPred = 0; }
            if (xPred + radius > env.GAME_WIDTH) { xPred = env.GAME_WIDTH - radius; sxPred = 0; }
            if (yPred + radius > env.GAME_HEIGHT) { yPred = env.GAME_HEIGHT - radius; syPred = 0; }

            Fragment pred = new Fragment(pId, fId, xPred, yPred, mass, radius, sxPred, syPred, ttf - nStraightTicks);

            return pred.predictFragmentByAngle(angle, nTicks - nStraightTicks, env);

        } else {

            double nx = Math.cos(angle);
            double ny = Math.sin(angle);

            double alpha = env.INERTION_FACTOR / mass;
            double alpha1 = 1.0 - alpha;
            double alpha2 = 1.0 - Math.pow(alpha1, nTicks);
            double alpha3 = alpha1 * alpha2 / alpha;

            double sxPred = sx * (1.0 - alpha2) + nx * maxSpeed * alpha2;
            double syPred = sy * (1.0 - alpha2) + ny * maxSpeed * alpha2;
            double s2 = Math.sqrt(sxPred * sxPred + syPred * syPred);
            if (s2 > maxSpeed) {
                sxPred *= maxSpeed / s2;
                syPred *= maxSpeed / s2;
            }

            double xPred = x + sx * alpha3 + nx * maxSpeed * (nTicks - alpha3);
            double yPred = y + sy * alpha3 + ny * maxSpeed * (nTicks - alpha3);
            if (xPred - radius < 0) { xPred = radius; sxPred = 0; }
            if (yPred - radius < 0) { yPred = radius; syPred = 0; }
            if (xPred + radius > env.GAME_WIDTH) { xPred = env.GAME_WIDTH - radius; sxPred = 0; }
            if (yPred + radius > env.GAME_HEIGHT) { yPred = env.GAME_HEIGHT - radius; syPred = 0; }

            return new Fragment(pId, fId, xPred, yPred, mass, radius, sxPred, syPred, ttf - nTicks);
        }
    }

    public int calcTicksToOvertake(double toX, double toY, GlobalParameters env) {

        double alpha = env.INERTION_FACTOR / mass;
        double alpha1 = 1.0 - alpha;
        double maxSpeed = env.SPEED_FACTOR / Math.sqrt(mass);

        int l = 0;
        int r = env.MAX_OVERTAKE_TICKS;

        while (l < r) {
            int k = (r + l) / 2;

            double alpha2 = 1.0 - Math.pow(alpha1, k);
            double alpha3 = alpha1 * alpha2 / alpha;
            double posX = x + sx * alpha3;
            double posY = y + sy * alpha3;
            double var = (k - alpha3) * maxSpeed;
            double dist = Geometry.distance(posX, posY, toX, toY);

            if (dist < var) {
                r = k;
            } else {
                l = k + 1;
            }
        }

        return l;
    }
}
