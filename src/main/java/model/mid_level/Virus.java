package model.mid_level;

public class Virus {

    private String id;
    private double mass;
    private double x;
    private double y;

    public Virus(String id, double mass, double x, double y) {
        this.id = id;
        this.mass = mass;
        this.x = x;
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public double getMass() {
        return mass;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
}
