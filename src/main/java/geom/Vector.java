package geom;

public class Vector {

    private double x;

    private double y;

    // Constructors

    public Vector() {
        this.x = 0;
        this.y = 0;
    }

    public Vector(Vector v) {
        this(v.x, v.y);
    }

    public Vector(double angleInRadians) {
        this(Math.cos(angleInRadians), Math.sin(angleInRadians));
    }

    public Vector(Point from, Point to) {
        this(to.getX() - from.getX(), to.getY() - from.getY());
    }

    public Vector(Point to) {
        this(to.getX(), to.getY());
    }

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    // Getter, setter

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    // NORM

    public double norm() {
        return Math.sqrt(norm2());
    }

    public double norm2() {
        return x * x + y * y;
    }

    // UPDATE THIS

    public void normalize() {
        double norm = norm();
        x /= norm;
        y /= norm;
    }

    public void mult(double c) {
        x *= c;
        y *= c;
    }

    public void add(Vector v) {
        x += v.x;
        y += v.y;
    }

    public void rotate(double angleInRadians) {
        double cosa = Math.cos(angleInRadians);
        double sina = Math.sin(angleInRadians);
        double tmp = x * cosa - y * sina;
        y = x * sina + y * cosa;
        x = tmp;
    }

    // CREATE NEW

    public Vector normalized() {
        double norm = norm();
        return new Vector(x / norm, y / norm);
    }

    public Vector neg() {
        return new Vector(-x, -y);
    }

    public Vector multed(double c) {
        return new Vector(x * c, y * c);
    }

    public Vector sum(Vector v) {
        return new Vector(x + v.x, y + v.y);
    }

    public Vector rotated(double angleInRadians) {
        double cosa = Math.cos(angleInRadians);
        double sina = Math.sin(angleInRadians);
        return new Vector(x * cosa - y * sina, x * sina + y * cosa);
    }

    // Vector x Vector

    public double dot(Vector v) {
        return x * v.x + y * v.y;
    }

    public double cross(Vector v) {
        return x * v.y - y * v.x;
    }
}
