package geom;

public class Point {

    private double x;

    private double y;


    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }


    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double dist(Point p) {
        return Math.sqrt(dist2(p.x, p.y));
    }

    public double dist(double x, double y) {
        return Math.sqrt(dist2(x, y));
    }

    public double dist2(Point p) {
        return dist2(p.x, p.y);
    }

    public double dist2(double x, double y) {
        return (this.x - x) * (this.x - x) + (this.y - y) * (this.y - y);
    }

    public Point shift(Vector v) {
        return shift(v.getX(), v.getY());
    }

    public Point shift(double x, double y) {
        return new Point(this.x + x, this.y + y);
    }
}
