package geom;

public class Geometry {

    public static double distance(double x, double y, double x1, double y1) {
        return Math.sqrt(distance2(x, y, x1, y1));
    }

    public static double distance2(double x, double y, double x1, double y1) {
        return (x1 - x) * (x1 - x) + (y1 - y) * (y1 - y);
    }

    public static double dotprod(double x, double y, double x1, double y1) {
        return x * x1 + y * y1;
    }

    public static double distanceFromPointToSegment(double x, double y, double x1, double y1, double x2, double y2) {
        double dist = 0;
        if (Double.compare(x1, x2) == 0 && Double.compare(y1, y2) == 0) {
            dist = distance(x, y, x1, y1);
        } else if (dotprod(x - x1, y - y1, x2 - x1, y2 - y1) <= 0) {
            dist = distance(x, y, x1, y1);
        } else if (dotprod(x - x2, y - y2, x1 - x2, y1 - y2) <= 0) {
            dist = distance(x, y, x2, y2);
        } else {
            dist = distanceFromPointToLine(x, y, x1, y1, x2, y2);
        }
        return dist;
    }

    public static double distanceFromPointToLine(double x, double y, double x1, double y1, double x2, double y2) {
        double r12 = distance(x1, y1, x2, y2);
        double s = (y2 - y1) * x - (x2 - x1) * y + x2 * y1 - y2 * x1;
        return Math.abs(s) / r12;
    }
}
