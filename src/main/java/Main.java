import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

import algo.PlayerAnalyzer;
import algo.Preprocessor;
import model.mid_level.Fragment;
import model.tiles.InfluenceMap;
import model.tiles.Tiles;
import algo.controllers.*;
import model.GlobalParameters;
import model.top_level.Mine;
import model.top_level.Objects;
import org.json.*;
import utils.logger.Logger;
import utils.logger.LoggerFactory;
import utils.visualization.Visualizator;
import utils.visualization.VisualizatorFactory;
import utils.visualization.ZWindow;

import static utils.Settings.FILE_INPUT_ENABLED;
import static utils.Settings.VISUALIZATION_ENABLED;

class Main {

    private static final Logger log = LoggerFactory.getLogger();
    private static final Visualizator vis = VisualizatorFactory.getVisualizator();
    private static ZWindow window;

    public static void main(String[] args) {

        BufferedReader in = new BufferedReader(inputStream());
        Preprocessor preprocessor = new Preprocessor();
        String line;

        try {
            line = in.readLine();
            JSONObject config = new JSONObject(line);
            log.info("Main.java: " + line);

            GlobalParameters env = preprocessor.getGlobalParameters(config);

            FarmController farmController = new FarmController(env);
            AttackController attackController = new AttackController(env);
            MainController mainController = new MainController(env, farmController, attackController);

            Mine mine = new Mine(env);
            Objects objects = new Objects();
            Tiles tiles = new Tiles(env);
            InfluenceMap influenceMap = new InfluenceMap(env, tiles);
            PlayerAnalyzer playerAnalyzer = new PlayerAnalyzer(env);

            if (VISUALIZATION_ENABLED) {
                window = new ZWindow(env);
                window.start();
            }

            if (FILE_INPUT_ENABLED) {
                line = in.readLine();
                line = in.readLine();
            }

            //List<Fragment> mineToLog = new ArrayList<>();
            //List<Fragment> predToLog = new ArrayList<>();

            while ((line = in.readLine()) != null && line.length() != 0) {

                JSONObject parsed = new JSONObject(line);
                JSONArray meJson = parsed.getJSONArray("Mine");
                JSONArray objJson = parsed.getJSONArray("Objects");
                log.info("Main.java: #" + objects.getTickIndex());
                log.info("Main.java: " + line);
                vis.update();

                preprocessor.updateMine(meJson, mine);
                env.update(mine);
                preprocessor.updateObjects(objJson, objects, mine, env);
                preprocessor.updateTiles(tiles, objects, mine);
                preprocessor.updateInfluenceMap(influenceMap, tiles, objects, mine);
                preprocessor.updatePlayerAnalyzer(playerAnalyzer, objects, mine);

                /*/
                mineToLog = new ArrayList<>(mine.getMe());
                mineToLog.sort(new Comparator<Fragment>() {
                    @Override
                    public int compare(Fragment o1, Fragment o2) {
                        int cmp1 = Double.compare(o1.getX(), o2.getX());
                        return cmp1 != 0 ? cmp1 : Double.compare(o1.getY(), o2.getY());
                    }
                });
                boolean isCorrectPredict = mineToLog.size() == predToLog.size();
                double eps = 1e-2;
                for (int i = 0; i < Math.min(mineToLog.size(), predToLog.size()); i++) {
                    Fragment f1 = mineToLog.get(i);
                    Fragment f2 = predToLog.get(i);
                    isCorrectPredict &= Math.abs(f1.getX() - f2.getX()) < eps && Math.abs(f1.getY() - f2.getY()) < eps;
                }
                if (!isCorrectPredict) {
                    StringBuilder predictLog = new StringBuilder("Actual : ");
                    for (int i = 0; i < mineToLog.size(); i++) {
                        predictLog.append(mineToLog.get(i).getX()).append(", ")
                                .append(mineToLog.get(i).getY()).append(", ")
                                .append(mineToLog.get(i).getMass()).append(";");
                    }
                    predictLog.append("\nPredict: ");
                    for (int i = 0; i < predToLog.size(); i++) {
                        predictLog.append(predToLog.get(i).getX()).append(", ")
                                .append(predToLog.get(i).getY()).append(", ")
                                .append(predToLog.get(i).getMass()).append(";");
                    }
                    log.info(predictLog.toString());
                }
                /*/

                JSONObject command = mainController.handle(objects, mine, influenceMap, tiles, playerAnalyzer);
                objects.incrementTickIndex();

                /*/
                predToLog = predictMineNextTick(mine, env);
                predToLog.sort(new Comparator<Fragment>() {
                    @Override
                    public int compare(Fragment o1, Fragment o2) {
                        int cmp1 = Double.compare(o1.getX(), o2.getX());
                        return cmp1 != 0 ? cmp1 : Double.compare(o1.getY(), o2.getY());
                    }
                });

                String foodLog = objects.getFoods().stream()
                        .map(f -> "(" + f.getX() + ", " + f.getY() + ") ")
                        .reduce("", String::concat);
                /*/

                if (mine.isFusePossible()) {
                    log.info(("Main.java: fuse possible"));
                    command.put("Debug", "fuse possible");
                }
                System.out.println(command.toString());

                if (VISUALIZATION_ENABLED) {
                    window.update(objects, mine, influenceMap, tiles);
                }

                if (FILE_INPUT_ENABLED) {
                    while ((line = in.readLine()) != null && line.length() != 0);
                    line = in.readLine();
                }
            }
        }
        catch (IOException e) {
            System.err.println(e);

        } finally {
            LoggerFactory.finish();
        }
    }

    private static List<Fragment> predictMineNextTick(Mine mine, GlobalParameters env) {

        log.info("Main.java: {'X': " + mine.getCommandX() + ", 'Y': " + mine.getCommandY() + "}");
        log.info("Main.java: split=" + mine.isSplit());

        if (mine.isSplit()) {
            return mine.predictMineSplit(mine.getCommandX(), mine.getCommandY()).getMe();
        } else {
            return mine.getMe().stream()
                    .map(f -> f.predictFragment(mine.getCommandX(), mine.getCommandY(), env))
                    .collect(Collectors.toList());
        }
    }

    private static Reader inputStream() {

        Reader in = null;

        if (FILE_INPUT_ENABLED) {
            try {
                in = new FileReader("D:\\Events\\MAIC\\dump.log");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            in = new InputStreamReader(System.in);
        }

        return in;
    }
}